  mount WillFilter::Engine => "/will_filter"

  resources :attaches,:except=>[:show] do 
  end

  resources :klassplanitems do 
    member do 
      post 'set_finished'
    end
  end

  get 'entrance/:id/finish_baoming_kpi'=>"entrance#finish_baoming_kpi",:as=>"entrance_finish_baoming_kpi"
# get "entrance/index"
  get 'reviews/search_by_phone'
  
  get "entrance/index"

  get "entrance/resources"

  # get "reviews/prepare"

  match "reviews/prepare"=>"reviews#prepare",:as=>"prepare_review"

  # match "friends/confirm_plan/:friend_id"=>"friends#confirm_plan",:as=>'friends_confirm_plan'

  root :to => "entrance#index"

  post "api/login"

  post "api/logout"

  post "api/notifications"

  post "api/notification"

  post "api/notificate"  

  post 'api/feedback'

  resources :markets do 
    resources :clients do
    end
  end

  resources :students do 
    resources :achieves do
      member do
        match :finish
        match :unfinish
      end
    end
  end

  resources :baomings_klassplanitems do
    member do
      match :plan
      match :wait_confirm
      match :confirm
      match :request_leave
      match :leave
      match :finish
      match :uncomplete
    end
  end
  resources :projects do

    resources :achieves

    resources :feedbacks

    resources :stats

    resources :entrance do
      collection do
        match 'index'
        match 'feedbacks'
      end
    end

    resources :notification_templates

    resources :seasons do 
      member do
        match :set_now
      end
    end

    

    resources :rules

    resources :baomings,:except=>[:new,:create] do
      member do
        match :pay
        # get 'projects/:id/finish_baoming_kpi'=>"entrance#finish_baoming_kpi",:as=>"entrance_finish_baoming_kpi"        match 'pay'
      end
    end

  	resources :markets

    resources :courses

    resources :services

  	resources :rooms do
      collection do
        match 'filter_rooms'
      end
      member do
        match 'duplicate'
      end
    end

  	resources :teachers do
      member do
        match 'duplicate'
      end
    end

  	resources :students do
      member do
        match 'duplicate'
        match 'to_client'
        match 'to_student'
        match 'to_alumna'
      end

      collection do
        delete 'destroy_multiple'
      end
    end

    resources :clients do
      member do
        match 'duplicate'
      end

      collection do
        delete 'destroy_multiple'
      end
    end

    resources :alumnas do
      member do
        match 'duplicate'
      end

      collection do
        delete 'destroy_multiple'
      end
    end

    resources :notifications do
      member do 
        match 'confirm'
        match 'unconfirm'
      end
    end

    resources :klassplans do
      member do 
        match 'start_plan'
        match 'confirm_plan'
        match 'ajax_menu'
        match 'ajax_kpis'
        match 'ajax_teachers'
        match 'ajax_rooms'
        match 'ajax_resources'
        match 'ajax_baoming'
        match 'wait_teacher_confirm'
      end

      collection do
        match 'planed_calendar'
        match 'planing_calendar'
        match 'autoplaning_calendar'
        match 'finished_calendar'
        match 'all_calendar'
        match 'auto'
        match 'start_auto'
        match 'dashborad'
      end
    end

    resources :reviews do
      member do
        match 'start'
        match 'set_feedbacked'
      end
      collection do
        get 'all'
        get 'my'
        get 'my_finished'
        get 'my_unfinished'
        get 'my_payed'
        match 'search_by_phone'
        match 'chose_reviewers'
        match 'available_reviewers'
      end
    end
  end

  resources :services

  resources :scores

  resources :warnings

  resources :people

  resources :students do
    resources :reviews
  end

  resources :teachers


  # The priority is based upon order of creation:
  # first created -> highest priority.

  # Sample of regular route:
  #   match 'products/:id' => 'catalog#view'
  # Keep in mind you can assign values other than :controller and :action

  # Sample of named route:
  #   match 'products/:id/purchase' => 'catalog#purchase', :as => :purchase
  # This route can be invoked with purchase_url(:id => product.id)

  # Sample resource route (maps HTTP verbs to controller actions automatically):
  #   resources :products

  # Sample resource route with options:
  #   resources :products do
  #     member do
  #       get 'short'
  #       post 'toggle'
  #     end
  #
  #     collection do
  #       get 'sold'
  #     end
  #   end

  # Sample resource route with sub-resources:
  #   resources :products do
  #     resources :comments, :sales
  #     resource :seller
  #   end

  # Sample resource route with more complex sub-resources
  #   resources :products do
  #     resources :comments
  #     resources :sales do
  #       get 'recent', :on => :collection
  #     end
  #   end

  # Sample resource route within a namespace:
  #   namespace :admin do
  #     # Directs /admin/products/* to Admin::ProductsController
  #     # (app/controllers/admin/products_controller.rb)
  #     resources :products
  #   end

  # You can have the root of your site routed with "root"
  # just remember to delete public/index.html.
  # root :to => 'welcome#index'

  # See how all your routes lay out with "rake routes"

  # This is a legacy wild controller route that's not recommended for RESTful applications.
  # Note: This route will make all actions in every controller accessible via GET requests.
  match ':controller(/:action(/:id))(.:format)'