class AddMenuToKlassplans < ActiveRecord::Migration
  def change
    add_column :klassplans, :menu, :string
    add_index :klassplans,:menu
  end
end
