class ChangeDatetimeToDate < ActiveRecord::Migration
  def up
  	change_column :reviews,:day,:date  	
  end

  def down
  	change_column :reviews,:day,:datetime  	
  end
end
