class AddEndAtToKlassplans < ActiveRecord::Migration
  def change
    add_column :klassplans, :end_at, :datetime
  end
end
