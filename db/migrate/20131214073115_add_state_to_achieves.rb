class AddStateToAchieves < ActiveRecord::Migration
  def change
    add_column :achieves, :state, :string
    add_index :achieves,:state
  end
end
