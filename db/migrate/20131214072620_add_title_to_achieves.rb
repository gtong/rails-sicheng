class AddTitleToAchieves < ActiveRecord::Migration
  def change
    add_column :achieves, :title, :string
  end
end
