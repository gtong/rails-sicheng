class AddProjectIdToRooms < ActiveRecord::Migration
  def change
  	add_column :rooms,:project_id,:integer
  	add_index :rooms,:project_id
  end
end
