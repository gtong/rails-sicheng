class ChangeColumnCandidates < ActiveRecord::Migration
  def up
  	change_column :candidates,:one_state,:string
  	change_column :candidates,:two_state,:string
  	change_column :candidates,:three_state,:string
  end

  def down
  	change_column :candidates,:one_state,:boolean
  	change_column :candidates,:two_state,:boolean
  	change_column :candidates,:three_state,:boolean
  end
end
