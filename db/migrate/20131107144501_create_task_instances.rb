class CreateTaskInstances < ActiveRecord::Migration
  def change
    create_table :task_instances do |t|
      t.integer :student_id
      t.string :title
      t.text :content
      t.string :task_instancable_type
      t.integer :task_instancable_id
      t.integer :score
      t.string :state

      t.timestamps
    end
  end
end
