class AddKlassTypeToKlassplan < ActiveRecord::Migration
  def change
    add_column :klassplans, :klass_type, :string
  end
end
