class CreateNotifications < ActiveRecord::Migration
  def change
    create_table :notifications do |t|
      t.string :notificatable_type
      t.integer :notificatable_id
      t.string :state
      t.string :title
      t.text :context

      t.timestamps
    end
  end
end
