class CreateCandidates < ActiveRecord::Migration
  def change
    create_table :candidates do |t|
      t.integer :student_id
      t.string :company
      t.string :position
      t.string :state
      t.datetime :applied_at
      t.datetime :passed_at

      t.timestamps
    end
  end
end
