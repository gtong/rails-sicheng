class AddProjectToKlass < ActiveRecord::Migration
  def change
    add_column :klasses, :project_id, :integer
    add_index :klasses,:project_id
  end
end
