class CreateNotificationTemplates < ActiveRecord::Migration
  def change
    create_table :notification_templates do |t|
      t.string :state
      t.string :title
      t.text :content
      t.integer :project_id

      t.timestamps
    end
  end
end
