class AddProjectIdToKlassplans < ActiveRecord::Migration
  def change
    add_column :klassplans, :project_id, :integer
  end
end
