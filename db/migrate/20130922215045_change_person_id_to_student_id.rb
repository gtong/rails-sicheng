class ChangePersonIdToStudentId < ActiveRecord::Migration
  def up
  	rename_column :baomings,:person_id,:student_id
  	add_column :baomings,:review_id,:integer
  end

  def down
  	rename_column :baomings,:student_id,:person_id
  	remove_column :baomings,:review_id,:integer
  end
end
