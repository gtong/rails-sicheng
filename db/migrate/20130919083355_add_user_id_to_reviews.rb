class AddUserIdToReviews < ActiveRecord::Migration
  def change
  	# add_column :reviews,:user_id,:integer
  	add_column :reviews,:project_id,:integer

  	# add_index :reviews,:user_id
  	add_index :reviews,:project_id
  end
end
