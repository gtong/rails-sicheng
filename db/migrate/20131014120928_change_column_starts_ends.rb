class ChangeColumnStartsEnds < ActiveRecord::Migration
  def up
  	[:mon_start,:mon_end,:tue_start,:tue_end,:wed_start,:wed_end,:thu_start,:thu_end,:fri_start,:fri_end,:sat_start,:sat_end,:sun_start,:sun_end].each do |key|
			change_column :rules,key,:string
  	end

  end

  def down
		[:mon_start,:mon_end,:tue_start,:tue_end,:wed_start,:wed_end,:thu_start,:thu_end,:fri_start,:fri_end,:sat_start,:sat_end,:sun_start,:sun_end].each do |key|
			change_column :rules,key,:datetime
  	end
  end
end
