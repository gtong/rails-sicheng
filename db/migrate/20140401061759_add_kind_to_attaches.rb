class AddKindToAttaches < ActiveRecord::Migration
  def change
    add_column :attaches, :kind, :string
  end
end
