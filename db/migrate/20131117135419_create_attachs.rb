class CreateAttachs < ActiveRecord::Migration
  def change
    create_table :attachs do |t|
      t.integer :container_id
      t.string :container_type
      t.string :file_file_name
      t.string :file_content_type
      t.integer :file_file_size
      t.datetime :file_uploaded_at

      t.timestamps
    end
  end
end
