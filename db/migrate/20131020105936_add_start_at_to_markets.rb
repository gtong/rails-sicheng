class AddStartAtToMarkets < ActiveRecord::Migration
  def change
    add_column :markets, :start_at, :datetime
    add_column :markets, :end_at, :datetime
  end
end
