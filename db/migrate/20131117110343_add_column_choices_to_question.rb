class AddColumnChoicesToQuestion < ActiveRecord::Migration
  def change
  	add_column :questions,:choice_a,:string
  	add_column :questions,:choice_b,:string
  	add_column :questions,:choice_c,:string
  	add_column :questions,:choice_d,:string
  	add_column :questions,:choice_e,:string
  	add_column :questions,:choice_f,:string
  	add_column :questions,:choice_g,:string
  	add_column :questions,:choice_h,:string
  	add_column :questions,:choice_i,:string
  	add_column :questions,:choice_j,:string

  	add_column :questions,:answer_a,:string
  	add_column :questions,:answer_b,:string
  	add_column :questions,:answer_c,:string
  	add_column :questions,:answer_d,:string
  	add_column :questions,:answer_e,:string
  	add_column :questions,:answer_f,:string
  	add_column :questions,:answer_g,:string
  	add_column :questions,:answer_h,:string
  	add_column :questions,:answer_i,:string
  	add_column :questions,:answer_j,:string
  end
end
