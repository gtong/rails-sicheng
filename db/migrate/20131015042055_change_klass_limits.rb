class ChangeKlassLimits < ActiveRecord::Migration
  def up
    change_column :klasses, :parents_finished_limit,:integer,:default=>0
    change_column :klasses, :parents_finished_or_learning_limit,:integer,:default=>0
    rename_column :klasses, :parents_learning_or_planed,:parents_learning_or_planed_limit
    change_column :klasses, :parents_learning_or_planed_limit,:integer,:default=>0
  end

  def down
    change_column :klasses, :parents_finished_limit,:integer
    change_column :klasses, :parents_finished_or_learning_limit,:integer
    change_column :klasses, :parents_learning_or_planed_limit,:integer
  end
end
