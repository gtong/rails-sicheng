class AddLunDescriptionToCandidate < ActiveRecord::Migration
  def change
  	(1..10).each do |i|
  		add_column :candidates,"lun#{i}_description",:string
  	end
  end
end
