class CreateKlassplanitems < ActiveRecord::Migration
  def change
    create_table :klassplanitems do |t|
      t.integer :klass_id
      t.integer :teacher_id
      t.integer :room_id
      t.datetime :start_at
      t.datetime :end_at

      t.timestamps
    end
  end
end
