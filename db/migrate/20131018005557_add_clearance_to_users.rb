class AddClearanceToUsers < ActiveRecord::Migration
  def self.up
    change_table :people  do |t|
      t.string :encrypted_password, :limit => 128
      t.string :confirmation_token, :limit => 128
      t.string :remember_token, :limit => 128
    end
    add_index :people, :remember_token
  end

  def self.down
    change_table :people do |t|
      t.remove :encrypted_password,:confirmation_token,:remember_token
    end
  end
end
