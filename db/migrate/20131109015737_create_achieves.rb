class CreateAchieves < ActiveRecord::Migration
  def change
    create_table :achieves do |t|
      t.string :company
      t.string :position
      t.string :duration
      t.text :job

      t.timestamps
    end
  end
end
