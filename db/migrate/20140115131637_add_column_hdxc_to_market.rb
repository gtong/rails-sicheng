class AddColumnHdxcToMarket < ActiveRecord::Migration
  def change
    add_column :markets, :hdxc, :datetime
  end
end
