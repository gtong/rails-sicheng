class ChangeColumnQq < ActiveRecord::Migration
  def up
  	change_column :people,:qq,:string
  end

  def down
  	change_column :people,:qq,:integer
  end
end
