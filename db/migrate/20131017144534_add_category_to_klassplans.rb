class AddCategoryToKlassplans < ActiveRecord::Migration
  def up
  	remove_column :klassplans,:menu
    add_column :klassplans, :category, :string
    add_index :klassplans,:category
  end

  def down
  end
end
