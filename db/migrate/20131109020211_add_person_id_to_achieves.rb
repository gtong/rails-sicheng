class AddPersonIdToAchieves < ActiveRecord::Migration
  def change
    add_column :achieves, :person_id, :integer
  end
end
