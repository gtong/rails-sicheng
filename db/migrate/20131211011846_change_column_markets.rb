class ChangeColumnMarkets < ActiveRecord::Migration
  def up
    change_column :markets,:qtsj,:string
    change_column :markets,:yjgm,:string
  end

  def down
    change_column :markets,:qtsj,:datetime
    change_column :markets,:yjgm,:integer
  end
end
