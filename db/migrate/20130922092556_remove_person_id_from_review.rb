class RemovePersonIdFromReview < ActiveRecord::Migration
  def up
  	remove_column :reviews,:person_id
  end

  def down
  	add_column :reviews,:person_id,:integer
  end
end
