class AddPriceToKlasses < ActiveRecord::Migration
  def change
    add_column :klasses, :price, :float
  end
end
