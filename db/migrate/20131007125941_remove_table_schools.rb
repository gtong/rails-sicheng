class RemoveTableSchools < ActiveRecord::Migration
  def up
  	drop_table :schools
  	drop_table :colleges
  	drop_table :majors
  end

  def down
  end
end
