class AddRequiredToNotification < ActiveRecord::Migration
  def change
    add_column :notifications, :required, :boolean
  end
end
