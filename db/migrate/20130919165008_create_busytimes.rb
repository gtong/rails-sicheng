class CreateBusytimes < ActiveRecord::Migration
  def change
    create_table :busytimes do |t|
      t.string :busyable_id
      t.string :busyable_type
      t.datetime :from
      t.datetime :to
    end
  end
end
