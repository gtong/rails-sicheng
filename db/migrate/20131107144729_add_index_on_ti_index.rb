class AddIndexOnTiIndex < ActiveRecord::Migration
  def up
  	add_index :task_instances,:state
  	add_index :task_instances,:score
  	add_index :task_instances,:task_instancable_type
  	add_index :task_instances,:task_instancable_id
  end

  def down
  	remove_index :task_instances,:state
  	remove_index :task_instances,:score
  	remove_index :task_instances,:task_instancable_type
  	remove_index :task_instances,:task_instancable_id
  end
end
