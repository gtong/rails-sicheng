class CreateBaomingsKlassplans < ActiveRecord::Migration
  def change
    create_table :baomings_klassplans do |t|
      t.integer :baoming_id
      t.integer :klassplan_id
      t.string :state
      t.timestamps
    end
  end
end
