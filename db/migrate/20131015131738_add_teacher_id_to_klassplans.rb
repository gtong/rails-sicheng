class AddTeacherIdToKlassplans < ActiveRecord::Migration
  def change
    add_column :klassplans, :teacher_id, :integer
  end
end
