class AddResponseToNotifications < ActiveRecord::Migration
  def change
    add_column :notifications, :response, :string
    add_index :notifications,:response
  end
end
