class AddColumnsToCandidates < ActiveRecord::Migration
  def change
  	add_column :candidates,:one_start_at,:string
  	add_column :candidates,:one_end_at,:string
  	add_column :candidates,:one_state,:boolean

  	add_column :candidates,:two_start_at,:string
  	add_column :candidates,:two_duration,:string
  	add_column :candidates,:two_state,:boolean

		add_column :candidates,:three_type,:string
  	add_column :candidates,:three_start_at,:string
  	add_column :candidates,:three_duration,:string
  	add_column :candidates,:three_state,:boolean
  end
end
