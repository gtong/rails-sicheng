class AddStateToKlassplanitems < ActiveRecord::Migration
  def change
    add_column :klassplanitems, :state, :string
    add_index :klassplanitems,:state
  end
end
