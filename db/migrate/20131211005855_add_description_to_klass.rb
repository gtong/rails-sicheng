class AddDescriptionToKlass < ActiveRecord::Migration
  def change
    add_column :klasses, :description, :text
  end
end
