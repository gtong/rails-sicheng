class AddRuleIdToKlasses < ActiveRecord::Migration
  def change
    add_column :klasses, :rule_id, :integer
  end
end
