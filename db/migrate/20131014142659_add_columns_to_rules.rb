class AddColumnsToRules < ActiveRecord::Migration
  def change
    add_column :rules, :week_max, :integer
    add_column :rules, :week_min, :integer
  end
end
