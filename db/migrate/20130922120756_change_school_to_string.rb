class ChangeSchoolToString < ActiveRecord::Migration
  def up
  	remove_column :people,:school_id
  	remove_column :people,:college_id
  	remove_column :people,:major_id

  	add_column :people,:school,:string
  	add_column :people,:college,:string
  	add_column :people,:major,:string

  	add_index :people,:school
  	add_index :people,:college
  	add_index :people,:major
  end

  def down
  	
  	add_column :people,:school_id,:integer
  	add_column :people,:college_id,:integer
  	add_column :people,:major_id,:integer

  	remove_column :people,:school
  	remove_column :people,:college
  	remove_column :people,:major
  end
end
