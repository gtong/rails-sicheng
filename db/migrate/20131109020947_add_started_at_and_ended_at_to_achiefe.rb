class AddStartedAtAndEndedAtToAchiefe < ActiveRecord::Migration
  def change
    add_column :achieves, :started_at, :datetime
    add_column :achieves, :ended_at, :datetime
  end
end
