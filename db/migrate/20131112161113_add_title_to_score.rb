class AddTitleToScore < ActiveRecord::Migration
  def change
    add_column :scores, :title, :string
  end
end
