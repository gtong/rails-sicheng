class RemoveStateFromKlasses < ActiveRecord::Migration
  def up
  	remove_column :klasses,:state
  end

  def down
  	add_column :klasses,:state,:string
  end
end
