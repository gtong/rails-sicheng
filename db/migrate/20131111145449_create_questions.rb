class CreateQuestions < ActiveRecord::Migration
  def change
    create_table :questions do |t|
      t.string :state
      t.string :title
      t.string :content
      t.text :answer
      t.integer :review_id

      t.timestamps
    end
  end
end
