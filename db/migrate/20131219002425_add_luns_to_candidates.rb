class AddLunsToCandidates < ActiveRecord::Migration
  def change
  	(1..10).each do |i|
	  	add_column :candidates,"lun#{i}_start_at",:string
	  	add_column :candidates,"lun#{i}_duration",:string
	  	add_column :candidates,"lun#{i}_state",:string
	  end
  end
end
