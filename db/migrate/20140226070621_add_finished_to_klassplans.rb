class AddFinishedToKlassplans < ActiveRecord::Migration
  def change
    add_column :klassplans, :finished, :boolean
  end
end
