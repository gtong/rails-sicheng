class ChangeGradeToFloat < ActiveRecord::Migration
  def up
  	change_column :people,:grade,:float
  end

  def down
  	change_column :people,:grade,:integer
  end
end
