class RemoveColumnParentIdFromKlass < ActiveRecord::Migration
  def up
  	remove_column :klasses,:parent_id
  end

  def down
  	add_column :klasses,:parent_id,:integer
  end
end
