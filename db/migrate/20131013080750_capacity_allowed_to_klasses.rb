class CapacityAllowedToKlasses < ActiveRecord::Migration
  def up
  	add_column :klasses,:capacity_allowed,:integer
  end

  def down
  	remove_column :klasses,:capacity_allowed
  end
end
