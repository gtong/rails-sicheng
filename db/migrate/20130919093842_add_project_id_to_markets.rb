class AddProjectIdToMarkets < ActiveRecord::Migration
  def change
    add_column :markets, :project_id, :integer
  end
end
