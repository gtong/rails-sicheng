class ClearTableQuestion2 < ActiveRecord::Migration
  def up
  	remove_column :questions,:answer_a
  	remove_column :questions,:answer_b
  	remove_column :questions,:answer_c
  	remove_column :questions,:answer_d
  	remove_column :questions,:answer_e
  	remove_column :questions,:answer_f
  	remove_column :questions,:answer_g
  	remove_column :questions,:answer_h
  	remove_column :questions,:answer_i
  	remove_column :questions,:answer_j
  end

  def down
  end
end
