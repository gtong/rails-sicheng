class AddJudgeChoiceToQuestions < ActiveRecord::Migration
  def change
    add_column :questions, :judge_choice, :boolean
  end
end
