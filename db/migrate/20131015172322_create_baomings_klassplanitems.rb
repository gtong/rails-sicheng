class CreateBaomingsKlassplanitems < ActiveRecord::Migration
  def change
    create_table :baomings_klassplanitems do |t|
      t.integer :baoming_id
      t.integer :klassplanitem_id
      t.string :state

      t.timestamps
    end
  end
end
