class AddPasswordAndLoginToPeople < ActiveRecord::Migration
  def change
    add_column :people, :password, :string
    add_column :people, :login, :string
  end
end
