class RemoveStateStudentFromTasks < ActiveRecord::Migration
  def up
  	remove_column :tasks,:student_id
  	remove_column :tasks,:state
  end

  def down
  	# remove_column :tasks,:student_id
  	# remove_column :tasks,:state
  end
end
