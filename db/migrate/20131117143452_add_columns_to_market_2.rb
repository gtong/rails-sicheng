class AddColumnsToMarket2 < ActiveRecord::Migration
  def change
  	add_column :markets,:leixing,:string
  	add_column :markets,:state,:string
  	add_column :markets,:hezuofang,:string
  	add_column :markets,:zhuti,:string
  	add_column :markets,:xingshi,:string
  	add_column :markets,:qtsj,:datetime
  	add_column :markets,:yjgm,:integer

  	add_column :markets,:zxqk,:string
  	add_column :markets,:dcrs,:string
  	add_column :markets,:xcxg,:string
  	add_column :markets,:wjdcjg,:string

  	add_column :markets,:hdht,:string
  	add_column :markets,:hdzp,:string
  	add_column :markets,:cg,:string
  	add_column :markets,:zj,:string
  	add_column :markets,:zzfk,:string
  end
end
