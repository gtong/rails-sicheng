class AddColumnChangePageType < ActiveRecord::Migration
  def up
  	change_column :questions,:page,:string
  end

  def down
  	change_column :questions,:page,:integer
  end
end
