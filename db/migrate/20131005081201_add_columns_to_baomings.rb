class AddColumnsToBaomings < ActiveRecord::Migration
  def change
    add_column :baomings, :free, :boolean
    add_column :baomings, :klass_id, :integer
    add_column :baomings, :end_at, :datetime
    add_index :baomings,:klass_id
    add_index :baomings,:free
  end
end
