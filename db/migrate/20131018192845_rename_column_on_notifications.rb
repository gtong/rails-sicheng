class RenameColumnOnNotifications < ActiveRecord::Migration
  def up
  	rename_column :notifications,:context,:content
  end

  def down
  	rename_column :notifications,:content,:context
  end
end
