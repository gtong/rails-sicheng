class AddOpenclassToMarkets < ActiveRecord::Migration
  def change
    add_column :markets, :openclass, :boolean,:default=>false
  end
end
