class AddProjectIdToPerson < ActiveRecord::Migration
  def change
  	add_column :people,:project_id,:boolean
  	add_index :people,:project_id
  end
end
