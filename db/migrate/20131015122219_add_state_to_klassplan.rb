class AddStateToKlassplan < ActiveRecord::Migration
  def change
  	add_column :klassplans,:state,:string
  	add_index :klassplans,:state
  end
end
