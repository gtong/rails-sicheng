class AddProcessToCandidate < ActiveRecord::Migration
  def change
    add_column :candidates, :process, :string
  end
end
