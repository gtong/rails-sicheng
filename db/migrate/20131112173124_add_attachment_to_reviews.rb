class AddAttachmentToReviews < ActiveRecord::Migration
  def change
  	add_column :reviews, :attachment_file_name, :string
    add_column :reviews, :attachment_content_type, :string
    add_column :reviews, :attachment_file_size, :integer
    add_column :reviews, :attachment_updated_at, :datetime
  end
end
