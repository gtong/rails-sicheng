class AddColumnsToKlasses < ActiveRecord::Migration
  def change
    add_column :klasses,:parents_finished_limit,:integer
    add_column :klasses,:parents_finished_or_learning_limit,:integer
    add_column :klasses,:parents_learning_or_planed,:integer
  end
end
