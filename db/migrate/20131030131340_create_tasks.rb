class CreateTasks < ActiveRecord::Migration
  def change
    create_table :tasks do |t|
      t.integer :student_id
      t.string :title
      t.text :content
      t.string :state

      t.timestamps
    end
  end
end
