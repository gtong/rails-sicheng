class ChangeRulesColumns < ActiveRecord::Migration
  def up

  	add_column :rules,:mon_start,:datetime
  	add_column :rules,:tue_start,:datetime
  	add_column :rules,:wed_start,:datetime
  	add_column :rules,:thu_start,:datetime
  	add_column :rules,:fri_start,:datetime
  	add_column :rules,:sat_start,:datetime
  	add_column :rules,:sun_start,:datetime

  	add_column :rules,:mon_end,:datetime
  	add_column :rules,:tue_end,:datetime
  	add_column :rules,:wed_end,:datetime
  	add_column :rules,:thu_end,:datetime
  	add_column :rules,:fri_end,:datetime
  	add_column :rules,:sat_end,:datetime
  	add_column :rules,:sun_end,:datetime

  	remove_column :rules,:mon
  	remove_column :rules,:tue
  	remove_column :rules,:wed
  	remove_column :rules,:thu
  	remove_column :rules,:fri
  	remove_column :rules,:sat
  	remove_column :rules,:sun
	end

  def down
  end
end
