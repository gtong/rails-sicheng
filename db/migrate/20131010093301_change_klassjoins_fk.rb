class ChangeKlassjoinsFk < ActiveRecord::Migration
  def up
  	rename_column :klassjoins,:klass_id,:child_id
  	rename_column :klassjoins,:front_id,:parent_id
  end

  def down
  	rename_column :klassjoins,:child_id,:klass_id
  	rename_column :klassjoins,:parent_id,:front_id
  end
end
