class ChangeProjectIdOfPerson < ActiveRecord::Migration
  def up
  	change_column :people,:project_id,:integer
  end

  def down
  	change_column :people,:project_id,:integer
  end
end
