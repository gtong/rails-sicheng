class ChangeRuleMaxMin < ActiveRecord::Migration
  def up
  	change_column :rules,:week_max,:integer,:default=>2
  	change_column :rules,:week_min,:integer,:default=>0
  end

  def down
  	change_column :rules,:week_max,:integer
  	change_column :rules,:week_min,:integer
  end
end
