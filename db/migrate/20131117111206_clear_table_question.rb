class ClearTableQuestion < ActiveRecord::Migration
  def up
  	remove_column :questions,:choices
  	remove_column :questions,:answer
  	remove_column :questions,:choice_a
  	remove_column :questions,:choice_b
  	remove_column :questions,:choice_c
  	remove_column :questions,:choice_d
  	remove_column :questions,:choice_e
  	remove_column :questions,:choice_f
  	remove_column :questions,:choice_g
  	remove_column :questions,:choice_h
  	remove_column :questions,:choice_i
  	remove_column :questions,:choice_j
  end

  def down
  end
end
