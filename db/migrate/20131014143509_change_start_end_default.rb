class ChangeStartEndDefault < ActiveRecord::Migration
  def up
  	[:mon_end,:tue_end,:wed_end,:thu_end,:fri_end,:sat_end,:sun_end].each do |key|
  		change_column :rules,key,:string,:default=>'18:00'
  	end

  	[:mon_start,:tue_start,:wed_start,:thu_start,:fri_start,:sat_start,:sun_start].each do |key|
  		change_column :rules,key,:string,:default=>'09:00'
  	end
  end

  def down
  	[:mon_end,:tue_end,:wed_end,:thu_end,:fri_end,:sat_end,:sun_end].each do |key|
  		change_column :rules,key,:string
  	end

  	[:mon_start,:tue_start,:wed_start,:thu_start,:fri_start,:sat_start,:sun_start].each do |key|
  		change_column :rules,key,:string
  	end
  end
end
