class AddColumnLunxTypeToCandidate < ActiveRecord::Migration
  def change

    add_column :candidates, :lun1_type, :string
    add_column :candidates, :lun2_type, :string
    add_column :candidates, :lun3_type, :string
    add_column :candidates, :lun4_type, :string
    add_column :candidates, :lun5_type, :string
    add_column :candidates, :lun6_type, :string
    add_column :candidates, :lun7_type, :string
    add_column :candidates, :lun8_type, :string
    add_column :candidates, :lun9_type, :string
    add_column :candidates, :lun10_type, :string
  end
end
