class AddRemarkToKlassplan < ActiveRecord::Migration
  def change
    add_column :klassplans, :remark, :text
  end
end
