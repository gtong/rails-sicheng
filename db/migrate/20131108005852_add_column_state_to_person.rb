class AddColumnStateToPerson < ActiveRecord::Migration
  def change
    add_column :people,:state,:string
    add_index :people,:state
  end
end
