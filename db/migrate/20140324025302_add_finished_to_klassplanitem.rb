class AddFinishedToKlassplanitem < ActiveRecord::Migration
  def change
    add_column :klassplanitems, :finished, :boolean
  end
end
