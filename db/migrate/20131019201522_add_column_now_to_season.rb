class AddColumnNowToSeason < ActiveRecord::Migration
  def change
    add_column :seasons, :now, :boolean
  end
end
