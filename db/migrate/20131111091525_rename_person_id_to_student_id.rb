class RenamePersonIdToStudentId < ActiveRecord::Migration
  def up
    rename_column :scores,:person_id,:student_id
  end

  def down
    rename_column :scores,:student_id,:person_id
  end
end
