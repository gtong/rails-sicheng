class AddDayToReview < ActiveRecord::Migration
  def change
    add_column :reviews, :day, :datetime
  end
end
