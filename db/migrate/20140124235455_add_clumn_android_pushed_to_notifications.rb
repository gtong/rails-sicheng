class AddClumnAndroidPushedToNotifications < ActiveRecord::Migration
  def change
    add_column :notifications, :android_pushed, :boolean,:default=>false
    add_column :notifications, :ios_pushed, :boolean,:default=>false
  end
end
