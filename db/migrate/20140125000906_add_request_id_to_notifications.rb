class AddRequestIdToNotifications < ActiveRecord::Migration
  def change
    add_column :notifications, :android_request_id, :string
    add_column :notifications, :ios_request_id, :string
  end
end
