class RenameAttachable < ActiveRecord::Migration
  def up
  	rename_column :attachs,:container_id,:attachable_id
  	rename_column :attachs,:container_type,:attachable_type
  end

  def down
  end
end
