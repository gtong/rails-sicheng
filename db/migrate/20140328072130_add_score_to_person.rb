class AddScoreToPerson < ActiveRecord::Migration
  def change
    add_column :people, :score, :float
  end
end
