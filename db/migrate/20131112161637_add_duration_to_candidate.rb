class AddDurationToCandidate < ActiveRecord::Migration
  def change
    add_column :candidates, :duration, :string
  end
end
