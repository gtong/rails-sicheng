class RemoveMenuIdFromKlasses < ActiveRecord::Migration
  def up
  	remove_column :klasses,:menu_id
  end

  def down
  	add_column :klasses,:menu_id,:integer
  end
end
