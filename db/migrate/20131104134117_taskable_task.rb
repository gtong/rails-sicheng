class TaskableTask < ActiveRecord::Migration
  def up
  	add_column :tasks,:taskable_type,:string
  	add_column :tasks,:taskable_id,:integer

  	add_index :tasks,:taskable_type
  	add_index :tasks,:taskable_id
  end

  def down
  	remove_column :tasks,:taskable_type,:string
  	remove_column :tasks,:taskable_id,:integer

  	remove_index :tasks,:taskable_type
  	remove_index :tasks,:taskable_id
  end
end
