class AddColumnIsServerNotifications < ActiveRecord::Migration
  def up
    add_column :notifications,:is_server,:boolean,:default=>false
  end

  def down
    remove_column :notifications,:is_server
  end
end
