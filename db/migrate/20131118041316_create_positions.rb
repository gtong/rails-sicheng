class CreatePositions < ActiveRecord::Migration
  def change
    create_table :positions do |t|
      t.string :title
      t.text :description
      t.integer :person_id
      t.timestamps
    end
  end
end
