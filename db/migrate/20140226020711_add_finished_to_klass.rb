class AddFinishedToKlass < ActiveRecord::Migration
  def change
    add_column :klasses, :finished, :boolean
  end
end
