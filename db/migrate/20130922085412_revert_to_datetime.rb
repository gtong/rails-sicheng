class RevertToDatetime < ActiveRecord::Migration
  def up
  	change_column :reviews,:start_at,:datetime
  	change_column :reviews,:end_at,:datetime
  end

  def down
  	change_column :reviews,:start_at,:time
  	change_column :reviews,:end_at,:time
  end
end
