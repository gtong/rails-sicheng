class AddPrioritiesToRules < ActiveRecord::Migration
  def change
    add_column :rules, :mon_prior, :integer,:default=>0
    add_column :rules, :tue_prior, :integer,:default=>0
    add_column :rules, :wed_prior, :integer,:default=>0
    add_column :rules, :thu_prior, :integer,:default=>0
    add_column :rules, :fri_prior, :integer,:default=>0
    add_column :rules, :sat_prior, :integer,:default=>0
    add_column :rules, :sun_prior, :integer,:default=>0
  end
end
