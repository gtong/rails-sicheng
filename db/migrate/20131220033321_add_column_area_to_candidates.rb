class AddColumnAreaToCandidates < ActiveRecord::Migration
  def change
    add_column :candidates, :area, :string
  end
end
