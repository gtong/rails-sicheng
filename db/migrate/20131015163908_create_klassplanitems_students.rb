class CreateKlassplanitemsStudents < ActiveRecord::Migration
  def change
    create_table :klassplanitems_students do |t|
      t.integer :klassplanitem_id
      t.integer :student_id
      t.string :state

      t.timestamps
    end
  end
end
