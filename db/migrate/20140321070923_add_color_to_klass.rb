class AddColorToKlass < ActiveRecord::Migration
  def change
    add_column :klasses, :color, :string
  end
end
