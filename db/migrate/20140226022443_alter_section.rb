class AlterSection < ActiveRecord::Migration
  def up
  	change_column :klasses,:section,:integer,:default=>1
  end

  def down
  	change_column :klasses,:section,:integer,:default=>nil
  end
end
