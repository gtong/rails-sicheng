# encoding: utf-8
Redmine::Plugin.register :sicheng do
	permission :students, { :students => [:index] }, :public => true
  
	# menu :project_menu, :reviews, { :controller => 'reviews', :action => 'index' }
  # permission :reviews, { :reviews => [:index, :new] }, :public => true
  # menu :project_menu, :reviews, { :controller => 'reviews', :action => 'index' }
  menu :project_menu, :markets, { :controller => 'markets', :action => 'index' }, :caption => '活动', :after => :files, :param => :project_id
  project_module :markets do
    permission :manage_markets, { :markets => [:index,:show,:new,:edit,:create,:update,:destroy,:import,:import_submit] }
  end

  menu :project_menu, :reviews, { :controller => 'reviews', :action => 'my' }, :caption => '评测', :after => :markets, :param => :project_id
  project_module :reviews do
    permission :manage_reviews, { :reviews => [:index,:all,:available_reviewers,:search_by_phone,:chose_reviewers,:prepare,:start,:show,:new,:edit,:create,:update,:destroy,:find_project] }
    permission :reviewers, { :reviews => [:set_feedbacked,:index,:start,:my,:my_unfinished,:my_payed,:my_finished],:rooms=>[:filter_rooms] }
  end  

  menu :project_menu, :klassplans, { :controller => 'klassplans', :action => 'index' }, :caption => '排课', :after => :reviews, :param => :project_id
  project_module :klassplans do
    permission :manage_klassplans, { :klassplans => [:confirm,:wait_teacher_confirm,:wait_confirm,:start_plan,:ajax_teachers,:ajax_baoming,:auto,:start_auto,:ajax_resources,:ajax_rooms,:ajax_menu,:ajax_kpis,:ajax_students,:dashborad,:confirm_plan,:index,:show,:new,:edit,:create,:update,:destroy,:planed_calendar,:planing_calendar,:autoplaning_calendar,:finished_calendar,:all_calendar,:new2,:create2] }
  end    

  menu :project_menu, :notifications, { :controller => 'notifications', :action => 'index' }, :caption => '通知', :after => :klassplans, :param => :project_id
  project_module :notifications do
    permission :manage_notifications, { :notifications => [:index,:show,:confirm,:unconfirm,:new,:create] }
  end      

  menu :project_menu, :clients, { :controller => 'clients', :action => 'index' }, :caption => '客户', :after => :notifications, :param => :project_id
  project_module :clients do
    permission :manage_clients, { :clients => [:destroy_multiple,:index,:show,:new,:edit,:create,:update,:destroy,:duplicate] }
  end

  menu :project_menu, :students, { :controller => 'students', :action => 'index' }, :caption => '学员', :after => :clients, :param => :project_id
  project_module :students do
    permission :manage_students, { :students => [
      :destroy_multiple,:index,:show,:edit,:create,:update,:destroy,:duplicate,
      :to_student,:to_client,:to_alumna
    ] 
    }
  end

  menu :project_menu, :alumnas, { :controller => 'alumnas', :action => 'index' }, :caption => '校友', :after => :students, :param => :project_id
  project_module :alumnas do
    permission :manage_alumnas, { :alumnas => [:destroy_multiple,:index,:show,:edit,:create,:update,:destroy,:duplicate] }
  end

  # menu :project_menu, :courses, { :controller => 'courses', :action => 'index' }, :caption => '课程', :after => :alumnas, :param => :project_id
  project_module :courses do
    permission :manage_courses, { :courses => [:index,:show,:new,:edit,:create,:update,:destroy,:import,:import_submit] }
  end

  # menu :project_menu, :services, { :controller => 'services', :action => 'index' }, :caption => '服务', :after => :courses, :param => :project_id
  project_module :services do
    permission :manage_services, { :services => [:index,:show,:new,:edit,:create,:update,:destroy,:import,:import_submit] }
  end


  # menu :project_menu, :rooms, { :controller => 'rooms', :action => 'index' }, :caption => '教室', :after => :services, :param => :project_id
  project_module :rooms do
    permission :manage_rooms, { :rooms => [:index,:show,:new,:edit,:create,:update,:destroy,:duplicate] }
  end


  menu :project_menu, :resources, { :controller => 'entrance', :action => 'resources' }, :caption => '资源库', :after => :alumnas, :param => :project_id
  project_module :resources do
    permission :manage_resources, { :entrance => [:resources] }
  end

  menu :project_menu, :feedbacks, { :controller => 'feedbacks', :action => 'index' }, :caption => '反馈', :after => :resources, :param => :project_id
  project_module :feedbacks do
    permission :manage_feedbacks, { :feedbacks => [:index] }
  end

  menu :project_menu, :stats, { :controller => 'stats', :action => 'index' }, :caption => '统计', :after => :feedbacks, :param => :project_id
  project_module :stats do
    permission :manage_stats, { :stats => [:index] }
  end

  # menu :project_menu, :teachers, { :controller => 'teachers', :action => 'index' }, :caption => '教师', :after => :stats, :param => :project_id
  project_module :teachers do
    permission :manage_teachers, { :teachers => [:index,:show,:new,:edit,:create,:update,:destroy,:duplicate] }
  end

  # menu :project_menu, :resources, { :controller => 'entrance', :action => 'resources' }, :caption => '资源', :after => :stat, :param => :project_id
  # project_module :resources do
  #   permission :reviewers, { :reviews => [:start,:my,:my_unfinished,:my_payed,:my_finished],:rooms=>[:filter_rooms] }
  # end

  # menu :project_menu, :my_reviews, { :controller => 'reviews', :action => 'my' }, :caption => '测评师', :after => :reviews, :param => :project_id


  

  # menu :project_menu, :seasons, { :controller => 'seasons', :action => 'index' }, :caption => '排课区间', :after => :klassplans, :param => :project_id
  # project_module :seasons do
  #   permission :manage_seasons, { :seasons => [:index,:show,:new,:edit,:create,:update,:destroy,:set_now] }
  # end  

  # menu :project_menu, :rules, { :controller => 'rules', :action => 'index' }, :caption => '排课策略', :after => :seasons, :param => :project_id
  # project_module :rules do
  #   permission :manage_rules, { :rules => [:index,:show,:new,:edit,:create,:update,:destroy] }
  # end  

  # menu :project_menu, :rules, { :controller => 'rules', :action => 'index' }, :caption => '排课策略', :after => :seasons, :param => :project_id
  # project_module :rules do
  #   permission :manage_rules, { :rules => [:index,:show,:new,:edit,:create,:update,:destroy] }
  # end

  # menu :project_menu, :notification_templates, { :controller => 'notification_templates', :action => 'index' }, :caption => '通知模板', :after => :rules, :param => :project_id
  # project_module :notification_templates do
  #   permission :manage_notification_templates, { :notification_templates => [:index,:show,:new,:edit,:create,:update,:destroy] }
  # end    

  

  

  # menu :project_menu, :entrances, { :controller => 'entrance', :action => 'index' }, :caption => '其他', :after => :stats, :param => :project_id
  # project_module :entrances do
  #   permission :view_feedbacks, { :entrance=>[:feedbacks] }
  #   permission :entrance_main, { :entrance=>[:index] }, :public => true
  #   permission :qiandao, { :entrance=>[:finish_baoming_kpi] },:public=>true
  # end


  name 'Sicheng plugin'
  author 'Gabriel Tong'
  description 'This is a plugin for Redmine'
  version '0.0.1'
  url 'http://redmine.edu58.cn/projects/qa'
  author_url 'http://example.com/about'

  def ppp(instance)
    p '>'*20
    p instance
    p '<'*20
  end

  class String
    def to_bool
      return true if ['true', '1', 'yes', 'on', 't'].include? self
      return false if ['false', '0', 'no', 'off', 'f'].include? self
      return nil
    end
  end
end
