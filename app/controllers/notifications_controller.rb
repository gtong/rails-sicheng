class NotificationsController < ApplicationController
	Notification
  before_filter :find_project,:authorize
  # GET /notifications
  # GET /notifications.json
  def index
    @notifications = Notification.page(params[:page]).order('created_at desc')
    respond_to do |format|
      format.html # index.html.erb
      format.json { render json: @notifications }
    end
  end

  # GET /notifications/1
  # GET /notifications/1.json
  def confirm
    notification = Notification.find(params[:id])
    notification.confirm
    notification.is_server = true
    notification.save
    redirect_to request.referer
  end

  def unconfirm
    notification = Notification.find(params[:id])
    notification.unconfirm
    notification.is_server = true
    notification.save
    redirect_to request.referer
  end


  # GET /notifications/1
  # GET /notifications/1.json
  def show
    @notification = Notification.find(params[:id])

    respond_to do |format|
      format.html # show.html.erb
      format.json { render json: @notification }
    end
  end

  # GET /notifications/new
  # GET /notifications/new.json
  def new
    begin
      gon.students_id = params[:students_id].split('-').map{|i|i.to_i}
    rescue
      gon.students_id = []
    end
    @notification = Notification.new

    respond_to do |format|
      format.html # new.html.erb
      format.json { render json: @notification }
    end
  end

  # GET /notifications/1/edit
  def edit
    @notification = Notification.find(params[:id])
  end

  # POST /notifications
  # POST /notifications.json
  def create
    params[:notification]['person_id'].each do |id|
      person = Person.find_by_id(id)
      if person
        @notification = Notification.new(params[:notification])
        @notification.person = person
        @notification.save
      end
    end
    redirect_to project_notifications_path(@project)
  end

  # PUT /notifications/1
  # PUT /notifications/1.json
  def update
    @notification = Notification.find(params[:id])

    respond_to do |format|
      if @notification.update_attributes(params[:notification])
        format.html { redirect_to project_notification_path(@project,@notification), notice: 'Notification was successfully updated.' }
        format.json { head :no_content }
      else
        format.html { render action: "edit" }
        format.json { render json: @notification.errors, status: :unprocessable_entity }
      end
    end
  end

  # DELETE /notifications/1
  # DELETE /notifications/1.json
  def destroy
    @notification = Notification.find(params[:id])
    @notification.destroy

    respond_to do |format|
      format.html { redirect_to project_notifications_url(@project) }
      format.json { head :no_content }
    end
  end
  private 

  def find_project
    # p params
    @project = Project.find_by_identifier(params[:project_id])
    # p @project
  end          
end

