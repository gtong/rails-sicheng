# encoding: utf-8
class StudentsController < ApplicationController
  Student

  before_filter :find_project, :authorize,:set_state
  # GET /student
  # GET /student.json
  def to_client
    @project.students.find(params[:id]).to_client
    redirect_to request.referer
  end

  def to_student
    @project.students.find(params[:id]).to_student
    redirect_to request.referer
  end

  def to_alumna
    @project.students.find(params[:id]).to_alumna
    redirect_to request.referer
  end

  def search
    render :json=>@project.students.student.where('phone'=>params[:phone]).select('username,qq,sex,id')
  end

  def index
    gon.new_notification_url = new_project_notification_path(@project)
    # @q = @project.students.student.order('created_at desc').search(params[:q])
    # @students = @q.result(distinct: true)
    # @students = .filter(:params => params,:filter => :student_filter) 
    
    @students = @project.students.student.order('created_at desc').filter(:params => params,:filter => :student_filter) 
    respond_to do |format|
      format.html # index.html.erb
      format.json { render json: @students }
    end
  end

  # GET /student/1
  # GET /student/1.json
  def show
    @student = @project.students.student.find(params[:id])
    @attachable = @student
    gon.calendar_events = @student.calendar_events
    respond_to do |format|
      format.html # show.html.erb
      format.json { render json: @student }
    end
  end

  # GET /student/new
  # GET /student/new.json
  def new
    @student = @project.students.student.new
    @attachable = @student
    respond_to do |format|
      format.html # new.html.erb
      format.json { render json: @student }
    end
  end

  # GET /student/1/edit
  def edit
    @student = @project.students.student.find(params[:id])
    @attachable = @student

    if @student.attaches.image.blank?
      @student.attaches << Attach.image.build
    end 
    @student.attaches.image.first.state = 'image'
  end

  # POST /student
  # POST /student.json
  def create
    # case params[:controller]
    # when 'students'
    @student = @project.students.new(params[:student])
    @attachable = @student
    # when 'clients'
    #   @student = @project.students.client.new(params[:student])
    # when 'alumnas'
    #   @student = @project.students.alumna.new(params[:student])
    # else
    # end
    @student.project = @project
    @student.markets << Market.find(params[:market][:id]) if params[:market] && Market.find_by_id(params[:market][:id])
    respond_to do |format|
      if @student.save
        if @student.client?
          format.html { redirect_to project_client_path(@project,@student), notice: '客户创建成功' }
        end
        if @student.student?
          format.html { redirect_to project_student_path(@project,@student), notice: '学员创建成功' }
        end
        if @student.alumna?
          format.html { redirect_to project_alumna_path(@project,@student), notice: '校友创建成功' }
        end

        format.json { render json: @student, status: :created, location: @student }
      else
        format.html { render action: "new" }
        format.json { render json: @student.errors, status: :unprocessable_entity }
      end
    end
  end

  # PUT /student/1
  # PUT /student/1.json
  def update
    # @student = @project.students.new(params[:student])
    @student = @project.students.find(params[:id])
    @attachable = @student
    # case params[:controller]
    # when 'students'
    #   @student = @project.students.student.new(params[:student])
    # when 'clients'
    #   p 'in'*100
    #   @student = @project.students.client.new(params[:student])
    # when 'alumnas'
    #   @student = @project.students.alumna.new(params[:student])
    # else
    # end

    # @student = @project.students.student.find(params[:id])

    respond_to do |format|
      if @student.update_attributes(params[:student])
        if @student.client?
          format.html { redirect_to project_client_path(@project,@student), notice: '客户更新成功' }
        end
        if @student.student?
          format.html { redirect_to project_student_path(@project,@student), notice: '学员更新成功' }
        end
        if @student.alumna?
          format.html { redirect_to project_alumna_path(@project,@student), notice: '校友更新成功' }
        end
        format.json { head :no_content }
      else
        if @student.client?
          @client = @student
          format.html { render :action=>:edit,:controller=>:clients }
        end
        if @student.student?
          format.html { render :action=>:edit,:controller=>:students }
        end
        if @student.alumna?
          @alumna = @student
          format.html { render :action=>:edit,:controller=>:alumnas }
        end
        
        format.json { render json: @student.errors, status: :unprocessable_entity }
      end
    end
  end

  # DELETE /student/1
  # DELETE /student/1.json
  def destroy
    @student = @project.students.student.find(params[:id])
    @student.destroy

    respond_to do |format|
      format.html { redirect_to project_student_path(@project,@student) }
      format.json { head :no_content }
    end
  end

  def duplicate
    @student = @project.students.student.find(params[:id])
    @student.duplicate
    redirect_to request.referer
  end

  def destroy_multiple

    @project.students.student.destroy(params[:students])

    redirect_to request.referer

  end

  private 

  def find_project
    # p params
    @project = Project.find_by_identifier(params[:project_id])
    # p @project
  end        

  def set_state
    @state = :student
  end
end
