class BaomingsKlassplanitemsController < ApplicationController
	def plan
		@bm_kpi = BaomingsKlassplanitem.find(params[:id]).plan
		redirect_to request.referer
  end

  def wait_confirm
  	@bm_kpi = BaomingsKlassplanitem.find(params[:id]).wait_confirm
  	redirect_to request.referer
  end

# 确认排课
  def confirm
  	@bm_kpi = BaomingsKlassplanitem.find(params[:id]).confirm
  	redirect_to request.referer
  end
# 请求请假
  def request_leave
  	@bm_kpi = BaomingsKlassplanitem.find(params[:id]).request_leave
  	redirect_to request.referer
  end
# 请假
  def leave
  	@bm_kpi = BaomingsKlassplanitem.find(params[:id]).leave
  	redirect_to request.referer
  end
# 签到
  def finish
  	@bm_kpi = BaomingsKlassplanitem.find(params[:id]).finish
  	redirect_to request.referer
  end
# 缺课
  def uncomplete
  	@bm_kpi = BaomingsKlassplanitem.find(params[:id]).uncomplete
  	redirect_to request.referer
  end
end
