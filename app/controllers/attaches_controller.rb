# encoding: utf-8
class AttachesController < ApplicationController
  before_filter :find_attachable
  # GET /attaches
  # GET /attaches.json
  def index
    @attaches = Attach.file.all

    respond_to do |format|
      format.html # index.html.erb
      format.json { render json: @attaches }
    end
  end

  # GET /attaches/1
  # GET /attaches/1.json
  def show

    @attach = Attach.file.find(params[:id])

    respond_to do |format|
      format.html # show.html.erb
      format.json { render json: @attach }
    end
  end

  # GET /attaches/new
  # GET /attaches/new.json
  def new
    @attach = Attach.file.new
    @attach.attachable = @attachable
    respond_to do |format|
      format.html # new.html.erb
      format.json { render json: @attach }
    end
  end

  # GET /attaches/1/edit
  def edit
    @attach = Attach.file.find(params[:id])
  end

  # POST /attaches
  # POST /attaches.json
  def create
    @attach = Attach.file.new(params[:attach])
    @student = @attach.attachable
    @project = @student.project
    respond_to do |format|
      if @attach.save
        if @student.client?
          format.html { redirect_to project_client_path(@project,@student), notice: '客户创建成功' }
        end
        if @student.student?
          format.html { redirect_to project_student_path(@project,@student), notice: '学员创建成功' }
        end
        if @student.alumna?
          format.html { redirect_to project_alumna_path(@project,@student), notice: '校友创建成功' }
        end
      else
        format.html { render action: "new" }
        format.json { render json: @attach.errors, status: :unprocessable_entity }
      end
    end
  end

  # PUT /attaches/1
  # PUT /attaches/1.json
  def update
    @attach = Attach.file.find(params[:id])

    respond_to do |format|
      if @attach.update_attributes(params[:attach])
        format.html { redirect_to @attach, notice: 'Attach was successfully updated.' }
        format.json { head :no_content }
      else
        format.html { render action: "edit" }
        format.json { render json: @attach.errors, status: :unprocessable_entity }
      end
    end
  end

  # DELETE /attaches/1
  # DELETE /attaches/1.json
  def destroy
    @attach = Attach.file.find(params[:id])
    @student = @attach.attachable
    @project = @student.project
    @attach.destroy

    respond_to do |format|
    if @student.client?
      format.html { redirect_to project_client_path(@project,@student), notice: '客户创建成功' }
    end
    if @student.student?
      format.html { redirect_to project_student_path(@project,@student), notice: '学员创建成功' }
    end
    if @student.alumna?
      format.html { redirect_to project_alumna_path(@project,@student), notice: '校友创建成功' }
    end
  end
  end

  def find_attachable
    if params[:attachable_type] == 'Student' || params[:attachable_type] == 'Person' || params[:attachable_type] == 'Client' || params[:attachable_type] == 'Alumna'
      @attachable = Person.find(params[:attachable_id])
    end
  end  
end
