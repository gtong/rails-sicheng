class TeachersController < ApplicationController
  Teacher
  before_filter :find_project, :authorize  
  # GET /teacher
  # GET /Teacher.json
  def index
    @teachers = @project.teachers.page(params[:page]).order('created_at desc')

    respond_to do |format|
      format.html # index.html.erb
      format.json { render json: @teachers }
    end
  end

  # GET /teacher/1
  # GET /teacher/1.json
  def show
    @teacher = @project.teachers.find(params[:id])
    gon.calendar_events = @teacher.calendar_events
    respond_to do |format|
      format.html # show.html.erb
      format.json { render json: @teacher }
    end
  end

  # GET /teacher/new
  # GET /teacher/new.json
  def new
    @teacher = @project.teachers.new

    respond_to do |format|
      format.html # new.html.erb
      format.json { render json: @teacher }
    end
  end

  # GET /teacher/1/edit
  def edit
    @teacher = @project.teachers.find(params[:id])
  end

  # POST /teacher
  # POST /Teacher.json
  def create
    @teacher = @project.teachers.new(params[:teacher])

    respond_to do |format|
      if @teacher.save
        format.html { redirect_to project_teacher_path(@project,@teacher), notice: 'teacher was successfully created.' }
        format.json { render json: @teacher, status: :created, location: @teacher }
      else
        format.html { render action: "new" }
        format.json { render json: @teacher.errors, status: :unprocessable_entity }
      end
    end
  end

  # PUT /teacher/1
  # PUT /teacher/1.json
  def update
    @teacher = @project.teachers.find(params[:id])

    respond_to do |format|
      if @teacher.update_attributes(params[:teacher])
        format.html { redirect_to project_teacher_path(@project,@teacher), notice: 'teacher was successfully updated.' }
        format.json { head :no_content }
      else
        format.html { render action: "edit" }
        format.json { render json: @teacher.errors, status: :unprocessable_entity }
      end
    end
  end

  # DELETE /teacher/1
  # DELETE /teacher/1.json
  def destroy
    @teacher = @project.teachers.find(params[:id])
    @teacher.destroy

    respond_to do |format|
      format.html { redirect_to project_teachers_path(@project) }
      format.json { head :no_content }
    end
  end

  def duplicate
    @teacher = @project.teachers.find(params[:id])
    @new = @teacher.dup
    @new.username = "#{@teacher.username} #{t :duplicate}"
    @new.save :validate=>false
    redirect_to request.referer
  end

  private 

  def find_project
    # p params
    @project = Project.find_by_identifier(params[:project_id])
    # p @project
  end        
end

