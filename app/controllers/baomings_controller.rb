class BaomingsController < ApplicationController
  Baoming
  before_filter :find_project
  # GET /baomings
  # GET /baomings.json
  def index
    @baomings = @project.baomings.order('created_at desc')

    respond_to do |format|
      format.html # index.html.erb
      format.json { render json: @baomings }
    end
  end

  # GET /baomings/1
  # GET /baomings/1.json
  def show
    @baoming = @project.baomings.find(params[:id])

    respond_to do |format|
      format.html # show.html.erb
      format.json { render json: @baoming }
    end
  end

  # GET /baomings/new
  # GET /baomings/new.json
  def new
    @baoming = @project.baomings.new

    respond_to do |format|
      format.html # new.html.erb
      format.json { render json: @baoming }
    end
  end

  # GET /baomings/1/edit
  def edit
    begin
      baomings_id = params[:baomings_id].split('-').map{|i|i.to_i}
      baomings_id.push params[:id] if baomings_id.blank?
      gon.baomings_id = baomings_id
    rescue
      gon.baomings_id = []
    end
    gon.baomings_id = [] if gon.baomings_id.blank?
    @baoming = @project.baomings.find(params[:id])
    @baomings = baomings_id.map do |i|
      Baoming.find(i)
    end
  end

  # POST /baomings
  # POST /baomings.json
  def create
    @baoming = @project.baomings.new(params[:baoming])

    respond_to do |format|
      if @baoming.save
        format.html { redirect_to project_baoming_path(@project,@baoming), notice: 'Baoming was successfully created.' }
        format.json { render json: @baoming, status: :created, location: @baoming }
      else
        format.html { render action: "new" }
        format.json { render json: @baoming.errors, status: :unprocessable_entity }
      end
    end
  end

  # PUT /baomings/1
  # PUT /baomings/1.json
  def update
    baomings_id = params[:baomings_id].split('-').map{|i|i.to_i}
    baomings_id.push params[:id] if baomings_id.blank?
    begin
      gon.baomings_id = baomings_id
    rescue
      gon.baomings_id = []
    end
    @baoming = @project.baomings.find(params[:id])
    @baomings = baomings_id.map do |i|
      Baoming.find(i)
    end

    @baoming = @project.baomings.find(params[:id])

    respond_to do |format|
      if @baoming.update_attributes(params[:baoming])
        @baomings.each do |bm|
          @bm.update_attributes(params[:baoming])
        end
        format.html { redirect_to project_baoming_path(@project,@baoming), notice: 'Baoming was successfully updated.' }
        format.json { head :no_content }
      else
        format.html { render action: "edit" }
        format.json { render json: @baoming.errors, status: :unprocessable_entity }
      end
    end
  end

  # DELETE /baomings/1
  # DELETE /baomings/1.json
  def destroy
    @baoming = @project.baomings.find(params[:id])
    @baoming.destroy

    respond_to do |format|
      format.html { redirect_to project_baomings_path(@project) }
      format.json { head :no_content }
    end
  end

  def pay
    if params[:baoming_pay]
      @pay = BaomingPay.new(params[:baoming_pay])
    else
      @pay = BaomingPay.new
      @pay.set_baomings Baoming.limit(3)
    end
  end

  private 

  def find_project
    # @project variable must be set before calling the authorize filter
    @project = Project.find_by_identifier(params[:project_id])
  end  

end
