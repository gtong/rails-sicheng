class ReviewsController < ApplicationController
  Review
  Busytime
  # GET /reviews
  # GET /reviews.json
  before_filter :find_project, :authorize

  def set_feedbacked
    @review = Review.find(params[:id])
    @review.set_feedbacked
    redirect_to request.referer
  end

  def index
  end


  def all
    # @reviews = User.current.reviews.by_project(@project)
    @reviews = Review.page(params[:page]).order('created_at desc')
    gon.calendar_events = Review.calendar_events
  end

  def my
    @reviews = User.current.reviews.by_project(@project).page(params[:page]).order('created_at desc')
  end

  def my_finished
    @reviews = User.current.reviews.by_project(@project).finished
  end

  def my_unfinished
    @reviews = User.current.reviews.by_project(@project).unfinished
  end

  def my_payed
    @reviews = User.current.reviews.by_project(@project).payed
  end

  def search_by_phone
    if params[:search_by_phone]
      @search_by_phone = SearchByPhone.new(params[:search_by_phone])

      if @search_by_phone.valid?
        student = @search_by_phone.search_student
        if student
          redirect_to chose_reviewers_project_reviews_path(@project,:phone=>@search_by_phone.phone)
        end
      end

    else
      @search_by_phone = SearchByPhone.new
    end
    # ppp @search_by_phone
  end

  def chose_reviewers
    @review = Review.new
    @student = Student.find_by_phone(params[:phone])
    redirect request.referer unless @student
  end

  def available_reviewers
    Busytime
    @collection = User.available_reviewers(params[:date].to_date,@project)
    respond_to do |format|
      format.html # index.html.erb
      format.js #index.js.erb
    end
  end

  def prepare
    # @student = Student.find_by_phone(params[:phone])
  end

  def start
    if params[:review]
      @review = Review.find(params[:review][:id])
      @review.update_attributes(params[:review])
      @review.start(params[:review][:courses_id])
      redirect_to project_review_path(@project,@review)
    else
      @review = Review.find(params[:id])
      @student = @review.student
      respond_to do |format|
        format.html  
        format.json { render json: @review }
      end
    end
  end

  # GET /reviews/1
  # GET /reviews/1.json
  def show
    @review = Review.find(params[:id])

    respond_to do |format|
      format.html # show.html.erb
      format.json { render json: @review }
    end
  end

  # GET /reviews/new
  # GET /reviews/new.json
  def new
    @review = Review.new
    @student = Student.find(params[:student_id]) if params[:student_id]
    respond_to do |format|
      format.html # new.html.erb
      format.json { render json: @review }
    end
  end

  # GET /reviews/1/edit
  def edit
    @review = Review.find(params[:id])
    @student = @review.student
  end

  # POST /reviews
  # POST /reviews.json
  def create
    @review = Review.new(params[:review])    
    respond_to do |format|
      if @review.save
        @student = @review.student
        @review.start_at = @review.start_at.change(:year=>@review.day.year,:month=>@review.day.mon,:day=>@review.day.day)
        @review.end_at = @review.end_at.change(:year=>@review.day.year,:month=>@review.day.mon,:day=>@review.day.day)
        @review.user_id = params[:review][:user_id]
        @review.student_id = params[:review][:student_id]
        @review.project = @project
        @review.busytime = Busytime.new(:from=>@review.start_at,:to=>@review.end_at)
        p @review.save
        p @review.errors
        p @review.id
        @review.import
        format.html { redirect_to project_review_path(@project,@review), notice: 'Review was successfully created.' }
        format.json { render json: @review, status: :created, location: @review }
      else
        format.html { render action: "new" }
        format.json { render json: @review.errors, status: :unprocessable_entity }
      end
    end
  end

  # PUT /reviews/1
  # PUT /reviews/1.json
  def update
    @review = Review.find(params[:id])

    respond_to do |format|
      if @review.update_attributes(params[:review])

        format.html { redirect_to project_review_path(@project,@review), notice: 'Review was successfully updated.' }
        format.json { head :no_content }
      else
        format.html { render action: "edit" }
        format.json { render json: @review.errors, status: :unprocessable_entity }
      end
    end
  end

  # DELETE /reviews/1
  # DELETE /reviews/1.json
  def destroy
    @review = Review.find(params[:id])
    @review.destroy

    respond_to do |format|
      format.html { redirect_to all_project_reviews_path(@project) }
      format.json { head :no_content }
    end
  end

  private 

  def find_project
    # @project variable must be set before calling the authorize filter
    @project = Project.find_by_identifier(params[:project_id])
  end  

  def do

  end
end
