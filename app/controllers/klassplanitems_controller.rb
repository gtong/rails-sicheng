class KlassplanitemsController < ApplicationController
  # GET /klassplanitems
  # GET /klassplanitems.json

  def set_finished
    finished = params[:finished]=="checked"
    @kpi = Klassplanitem.find(params[:id])
    @kpi.finished = finished
    @kpi.save :validate=>false
    render :json=>{}
  end

  def index
    @klassplanitems = Klassplanitem.order('created_at desc')

    respond_to do |format|
      format.html # index.html.erb
      format.json { render json: @klassplanitems }
    end
  end

  # GET /klassplanitems/1
  # GET /klassplanitems/1.json
  def show
    @klassplanitem = Klassplanitem.find(params[:id])
    @project = @klassplanitem.klassplan.project
    respond_to do |format|
      format.html # show.html.erb
      format.json { render json: @klassplanitem }
    end
  end

  # GET /klassplanitems/new
  # GET /klassplanitems/new.json
  def new
    @klassplanitem = Klassplanitem.new

    respond_to do |format|
      format.html # new.html.erb
      format.json { render json: @klassplanitem }
    end
  end

  # GET /klassplanitems/1/edit
  def edit
    @klassplanitem = Klassplanitem.find(params[:id])
  end

  # POST /klassplanitems
  # POST /klassplanitems.json
  def create
    @klassplanitem = Klassplanitem.new(params[:klassplanitem])

    respond_to do |format|
      if @klassplanitem.save
        format.html { redirect_to @klassplanitem, notice: 'Klassplanitem was successfully created.' }
        format.json { render json: @klassplanitem, status: :created, location: @klassplanitem }
      else
        format.html { render action: "new" }
        format.json { render json: @klassplanitem.errors, status: :unprocessable_entity }
      end
    end
  end

  # PUT /klassplanitems/1
  # PUT /klassplanitems/1.json
  def update
    @klassplanitem = Klassplanitem.find(params[:id])

    respond_to do |format|
      if @klassplanitem.update_attributes(params[:klassplanitem])
        format.html { redirect_to @klassplanitem, notice: 'Klassplanitem was successfully updated.' }
        format.json { head :no_content }
      else
        format.html { render action: "edit" }
        format.json { render json: @klassplanitem.errors, status: :unprocessable_entity }
      end
    end
  end

  # DELETE /klassplanitems/1
  # DELETE /klassplanitems/1.json
  def destroy
    @klassplanitem = Klassplanitem.find(params[:id])
    @klassplanitem.destroy

    respond_to do |format|
      format.html { redirect_to klassplanitems_url }
      format.json { head :no_content }
    end
  end

end
