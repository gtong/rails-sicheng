class RoomsController < ApplicationController
  Room
  before_filter :find_project, :authorize
  # GET /rooms
  # GET /rooms.json
  def index
    @rooms = @project.rooms.page(params[:page]).order('created_at desc')

    respond_to do |format|
      format.html # index.html.erb
      format.json { render json: @rooms }
    end
  end

  # GET /rooms/1
  # GET /rooms/1.json
  def show
    @room = @project.rooms.find(params[:id])
    gon.calendar_events = @room.calendar_events
    respond_to do |format|
      format.html # show.html.erb
      format.json { render json: @room }
    end
  end

  # GET /rooms/new
  # GET /rooms/new.json
  def new
    @room = @project.rooms.new

    respond_to do |format|
      format.html # new.html.erb
      format.json { render json: @room }
    end
  end

  # GET /rooms/1/edit
  def edit
    @room = @project.rooms.find(params[:id])
  end

  # POST /rooms
  # POST /rooms.json
  def create
    @room = @project.rooms.new(params[:room])

    respond_to do |format|
      if @room.save
        format.html { redirect_to project_room_path(@project,@room), notice: 'Room was successfully created.' }
        format.json { render json: @room, status: :created, location: @room }
      else
        format.html { render action: "new" }
        format.json { render json: @room.errors, status: :unprocessable_entity }
      end
    end
  end

  # PUT /rooms/1
  # PUT /rooms/1.json
  def update
    @room = @project.rooms.find(params[:id])

    respond_to do |format|
      if @room.update_attributes(params[:room])
        format.html { redirect_to project_room_path(@project,@room), notice: 'Room was successfully updated.' }
        format.json { head :no_content }
      else
        format.html { render action: "edit" }
        format.json { render json: @room.errors, status: :unprocessable_entity }
      end
    end
  end

  # DELETE /rooms/1
  # DELETE /rooms/1.json
  def destroy
    @room = @project.rooms.find(params[:id])
    @room.destroy

    respond_to do |format|
      format.html { redirect_to project_rooms_path(@project) }
      format.json { head :no_content }
    end
  end

  def filter_rooms
    relation = @project.rooms

    relation = relation.where(:mic=>true) if params[:mic] == 'true'
    relation = relation.where(:projector=>true) if params[:projector] == 'true'
    relation = relation.where(:sound=>true) if params[:sound] == 'true'
    relation = relation.where(:network=>true) if params[:network] == 'true'
    relation = relation.where(:laser=>true) if params[:laser] == 'true'
    
    @rooms = relation

    respond_to do |format|
      format.html # index.html.erb
      format.js #index.js.erb
    end
  end

  def duplicate
    @room = @project.rooms.find(params[:id])
    @new = @room.dup
    @new.title = "#{@room.title} #{t :duplicate}"
    @new.save :validate=>false
    redirect_to request.referer
  end

  private 

  def find_project
    # p params
    @project = Project.find_by_identifier(params[:project_id])
    # p @project
  end      
end

