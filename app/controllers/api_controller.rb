class ApiController < ApplicationController
  layout :resolve_layout
  Review
  before_filter :get_student,:get_teacher,:get_person,:paginate_params,:get_candidate,:get_user,:get_review

  skip_before_filter :verify_authenticity_token
#登陆接口
  def login
    person = Person.find_by_phone_and_password(params[:phone],params[:password])
    if person
      attributes = person.attributes
      attributes[:avatar_url] = person.attaches.image.first.nil? ? '' : person.attaches.image.first.file.url
      attributes[:type] = person.class.to_s
      attributes[:score] = person.scores.collect {|s|s.value}.inject(:+)
      attributes[:klassplans_count] = person.klassplans.count()
      attributes[:achieves_count] = person.achieves.count()
      attributes[:finished_klassplans_count] = person.klassplans.finished.count()
      attributes[:finished_task_instances_count] = person.task_instances.finished.count()
      attributes[:finished_achieves_count] = person.achieves.finished.count()
      attributes[:achieves_count] = person.achieves.count()
      attributes['qzsj'] = attributes['qzsj'].nil? ? '' : attributes['qzsj'].to_formatted_s(:db)
    else
      attributes = {}
    end
    render :json=>attributes
  end

  def kpis
    render :json=>Person.find_by_id(params[:person_id]).klassplanitems.where(:start_at=>Time.now.to_date..(Time.now.to_date+1.day)).to_json(:include=>[:teacher,:room,:klass])
  end

  def logout
  end

  def reviews_list
    @relation = @user.reviews.order('created_at desc')
    paginate
    render :json=>@collection.to_json({:include=>{:student=>{}}})
  end

  def review
    @review = Review.last
    @project = @review.project
    @student = @review.student
  end
#通知列表
  def notifications
    @relation = Notification.where("person_id=?",params[:person_id]).order('created_at desc')
    paginate
    render :json=>@collection
  end

#通知详情
  def notification
    render :json=>Notification.where(:id=>params[:id]).order('created_at desc').to_json(:include=>[:notificatable])
  end

#通知反馈
  def notificate
    notification = Person.find(params[:person_id]).notifications.find(params[:notification_id])
    if notification.handle_response(params[:response])
      render_success
    else
      render_fail
    end
  end

#添加成就
  def create_achiefe
    a = Achiefe.new(params[:achiefe])
    a.person = @person
    a.valid?
    render :json=>a.save
  end

#成就列表
  def achieves
    render :json=>@person.achieves.order('created_at desc')
  end

#任务列表
  def tasks
    render :json=>@person.task_instances.order('created_at desc')
  end

#完成任务
  def finish_task
    if TaskInstance.find(params[:task_id]).finish
      render_success
    else
      render_fail
    end
  end

#课表接口
  def klassplans
    data = JSON.parse(@student.klassplans.order('created_at desc').to_json(
      :include=>{
        :klassplanitems=>{
          :include=>{
            :teacher=>{},
            :room=>{}
          }
        },
        :klass=>{}
      }
    ))
    data[0]['klassplan']['klassplanitems'] = data[0]['klassplan']['klassplanitems'].map do |record|
      record['person_state'] = @student.baomings_klassplanitems.where('klassplanitem_id'=>record['id']).first.try(:state)
      record
    end
    render :json=>data.to_json
  end
# 创建岗位
  def create_position
    a = Position.new(params[:position])
    a.person = @person
    if a.save
      render_success
    else
      render_fail
    end
  end

#岗位列表
  def positions
    render :json=>@person.positions.order('created_at desc')
  end
#添加应聘
  def create_candidate
    a = Candidate.new(params[:candidate])
    a.student = @student
    if a.save
      render_success
    else
      render_fail
    end
  end

#通过应聘
  def pass_candidate
    if @student.candidates.find(params[:candidate_id]).pass
      render_success
    else
      render_fail
    end
  end

#通过应聘
  def update_candidate
    if @student.candidates.find(params[:candidate][:id]).update_attributes(params[:candidate])
      render_success
    else
      render_fail
    end
  end  

#通过应聘
  def unpass_candidate
    if @student.candidates.find(params[:candidate_id]).unpass
      render_success
    else
      render_fail
    end
  end  

#应聘列表
  def candidates
    render :json=>@student.candidates.order('created_at desc')
  end

# 用户积分
  def scores
    render :json=>@student.scores.order('created_at desc')
  end

  def leave_klassplanitem
    if @student.baomings_klassplanitems.where('klassplanitem_id'=>params[:klassplanitem_id]).first.try(:leave)
      render_success
    else
      render_fail
    end
  end
#所有课程与服务接口
  def klasses
    @student = Student.first
    # 属性结构
    # records = {:services=>{},:courses=>{}}
    # Service.all.each do |service|
    #   unless records[:services][service.menu]
    #     records[:services][service.menu] = [service]
    #   else
    #     records[:services][service.menu].push service
    #   end
    # end

    # Course.all.each do |course|
    #   unless records[:courses][course.menu]
    #     records[:courses][course.menu] = [course]
    #   else
    #     records[:courses][course.menu].push course
    #   end
    # end

    # render :json=>records
    service_attributes = []
    klasses = @student.klasses
    # klasses = 
    Service.order('created_at desc').each do |service|
      attributes = service.attributes
      attributes[:learned] = klasses.include?service
      service_attributes.push attributes
    end

    course_attributes = []
    klasses = @student.klasses
    Course.order('created_at desc').each do |course|
      attributes = course.attributes
      attributes[:learned] = klasses.include?course
      course_attributes.push attributes
    end
    render :json=>{
      :services=>service_attributes.group_by {|x|x['menu']},
      :courses=>course_attributes.group_by {|x|x['menu']}
    }
  end
  # def klass
  #   render :json=>Klass.find(params[:person_id])
  # end
  #反馈接口
  def feedback
    fb = Feedback.new(params[:feedback])
    fb.student = @student
    fb.valid?
    if fb.save
      render_success
    else
      render_fail
    end
  end

# common task begin
  def duplicate
    redirect_to request.referer
  end

  private 

#全局得到人学员老师
  def get_person
    @person = Person.find_by_id(params[:person_id]) if params[:person_id]
  end

  def get_student
    @student = Student.find_by_id(params[:student_id]) if params[:student_id]
  end

  def get_teacher
    @teacher = Teacher.find_by_id(params[:teacher_id]) if params[:teacher_id]
  end

  def get_candidate
    @candidate = Candidate.find_by_id(params[:candidate_id]) if params[:candidate_id]
  end

  def get_user
    @user = User.find_by_id(params[:user_id]) if params[:user_id]
    @user = User.find(1) unless @user
  end

  def get_review
    @review = Review.find_by_id(params[:review_id]) if params[:review_id]
  end
  # 在返回集合的api上设置分页的页数和分页大小
  # 结果：设置好 @page 和 @per_page
  def paginate_params
    @page = params[:page] || 1 
    @per_page = params[:per_page] || 100
    @random = params[:random].to_i || 0
  end

  # 根据分页的数量
  # require @page
  # require @per_page
  # set @collection
  def paginate
    if @random == 0
      @collection = @relation.page(@page)
    else
      @collection = @relation.random(@per_page)
    end
  end

  def resolve_layout
    case action_name
    when 'review'
      'bootstrap'
    else
      'application'
    end
  end

  def render_success(msg='')
    render json: {:result=>:success,:msg=>msg}
  end

  def render_fail(msg=nil)
    render json: {:result=>:fail,:msg=>msg}
  end    
end
