# encoding: utf-8
class AchievesController < ApplicationController
  # GET /achieves
  # GET /achieves.json
  Achiefe
  before_filter :find_project,:find_person

  def finish
    @achiefe = Achiefe.find(params[:id])
    @achiefe.finish
    redirect_to request.referer
  end

  def unfinish
    @achiefe = Achiefe.find(params[:id])
    @achiefe.unfinish
    redirect_to request.referer
  end

  def index
    @achieves = Achiefe.order('created_at desc')

    respond_to do |format|
      format.html # index.html.erb
      format.json { render json: @achieves }
    end
  end

  # GET /achieves/1
  # GET /achieves/1.json
  def show
    @achiefe = Achiefe.find(params[:id])

    respond_to do |format|
      format.html # show.html.erb
      format.json { render json: @achiefe }
    end
  end

  # GET /achieves/new
  # GET /achieves/new.json
  def new
    @achiefe = Achiefe.new
    @achiefe.person = @person if @person

    respond_to do |format|
      format.html # new.html.erb
      format.json { render json: @achiefe }
    end
  end

  # GET /achieves/1/edit
  def edit
    @achiefe = Achiefe.find(params[:id])
  end

  # POST /achieves
  # POST /achieves.json
  def create
    @achiefe = Achiefe.new(params[:achiefe])

    respond_to do |format|
      if @achiefe.save
        @person = @achiefe.person
        if @person.client?
          format.html { redirect_to project_client_path(@project,@person), notice: '成就创建成功' }
        end
        if @person.student?
          format.html { redirect_to project_student_path(@project,@person), notice: '成就创建成功' }
        end
        if @person.alumna?
          format.html { redirect_to project_alumna_path(@project,@person), notice: '成就创建成功' }
        end
      else
        format.html { render action: "new" }
        format.json { render json: @achiefe.errors, status: :unprocessable_entity }
      end
    end
  end

  # PUT /achieves/1
  # PUT /achieves/1.json
  def update
    @achiefe = Achiefe.find(params[:id])

    respond_to do |format|
      if @achiefe.update_attributes(params[:achiefe])
        format.html { redirect_to @achiefe, notice: 'Achiefe was successfully updated.' }
        format.json { head :no_content }
      else
        format.html { render action: "edit" }
        format.json { render json: @achiefe.errors, status: :unprocessable_entity }
      end
    end
  end

  # DELETE /achieves/1
  # DELETE /achieves/1.json
  def destroy
    @achiefe = Achiefe.find(params[:id])
    @achiefe.destroy

    respond_to do |format|
      format.html { redirect_to achieves_url }
      format.json { head :no_content }
    end
  end

  private 

  def find_project
    # p params
    @project = Project.find_by_identifier(params[:project_id])
    # p @project
  end          

  def find_person
    # p params
    @person = Person.find_by_id(params[:person_id])
    p '.'*10
    p @person
    # p @project
  end            
end
