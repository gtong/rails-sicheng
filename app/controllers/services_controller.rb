class ServicesController < ApplicationController
  Course
  Teacher
  Room
  Service
  before_filter :find_project, :authorize
  
  def index
    @services = @project.services.page(params[:page]).order('created_at desc')

    respond_to do |format|
      format.html 
      format.json { render json: @services }
    end
  end

  def show
    @service = @project.services.find(params[:id])
    gon.calendar_events = @service.calendar_events
    respond_to do |format|
      format.html # show.html.erb
      format.json { render json: @service }
    end
  end

  def new
    @service = @project.services.new
    1.times do 
      @service.tasks.build
    end    
    respond_to do |format|
      format.html # new.html.erb
      format.json { render json: @service }
    end
  end


  def edit
    @service = @project.services.find(params[:id])
  end

  def create
    @service = @project.services.new(params[:service])

    respond_to do |format|
      if params[:rooms_id]
        @service.rooms = []
        params[:rooms_id].each do |room_id|
          @service.rooms << @project.rooms.find(room_id)
        end
      end
      if params[:teachers_id]
        @service.teachers = []
        params[:teachers_id].each do |teacher_id|
          @service.teachers << @project.teachers.find(teacher_id)
        end
      end
      if @service.save
        format.html { redirect_to project_service_path(@project,@service), notice: 'Service was successfully created.' }
        format.json { render json: @service, status: :created, location: @service }
      else
        format.html { render action: "new" }
        format.json { render json: @service.errors, status: :unprocessable_entity }
      end
    end
  end

  def update
    @service = @project.services.find(params[:id])

    respond_to do |format|
      if params[:rooms_id]
        @service.rooms = []
        params[:rooms_id].each do |room_id|
          @service.rooms << @project.rooms.find(room_id)
        end
      end
      if params[:teachers_id]
        @service.teachers = []
        params[:teachers_id].each do |teacher_id|
          @service.teachers << @project.teachers.find(teacher_id)
        end
      end
      if @service.update_attributes(params[:service])
        format.html { redirect_to project_service_path(@project,@service), notice: 'Service was successfully updated.' }
        format.json { head :no_content }
      else
        format.html { render action: "edit" }
        format.json { render json: @service.errors, status: :unprocessable_entity }
      end
    end
  end

  def destroy
    @service = @project.services.find(params[:id])
    @service.destroy

    respond_to do |format|
      format.html { redirect_to project_services_path(@project) }
      format.json { head :no_content }
    end
  end

  private 

  def find_project
    # p params
    @project = Project.find_by_identifier(params[:project_id])
    # p @project
  end    
end
