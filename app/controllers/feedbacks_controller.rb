class FeedbacksController < ApplicationController
	before_filter :find_project	
  def index
  	@relation = Feedback.order('id desc')
  	# paginate
  	@collection = @relation
  end
  private 

  def find_project
    # p params
    @project = Project.find_by_identifier(params[:project_id])
    # p @project
  end          
end
