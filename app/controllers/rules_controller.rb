class RulesController < ApplicationController
  Rule
  before_filter :find_project, :authorize      
  # GET /rules
  # GET /rules.json
  def index
    @rules = @project.rules.order('created_at desc')

    respond_to do |format|
      format.html # index.html.erb
      format.json { render json: @rules }
    end
  end

  # GET /rules/1
  # GET /rules/1.json
  def show
    @rule = @project.rules.find(params[:id])

    respond_to do |format|
      format.html # show.html.erb
      format.json { render json: @rule }
    end
  end

  # GET /rules/new
  # GET /rules/new.json
  def new
    @rule = @project.rules.new

    respond_to do |format|
      format.html # new.html.erb
      format.json { render json: @rule }
    end
  end

  # GET /rules/1/edit
  def edit
    @rule = @project.rules.find(params[:id])
  end

  # POST /rules
  # POST /rules.json
  def create
    @rule = @project.rules.new(params[:rule])

    respond_to do |format|
      if @rule.save
        format.html { redirect_to project_rule_path(@project,@rule), notice: 'Rule was successfully created.' }
        format.json { render json: @rule, status: :created, location: @rule }
      else
        format.html { render action: "new" }
        format.json { render json: @rule.errors, status: :unprocessable_entity }
      end
    end
  end

  # PUT /rules/1
  # PUT /rules/1.json
  def update
    @rule = @project.rules.find(params[:id])

    respond_to do |format|
      if @rule.update_attributes(params[:rule])
        format.html { redirect_to project_rule_path(@project,@rule), notice: 'Rule was successfully updated.' }
        format.json { head :no_content }
      else
        format.html { render action: "edit" }
        format.json { render json: @rule.errors, status: :unprocessable_entity }
      end
    end
  end

  # DELETE /rules/1
  # DELETE /rules/1.json
  def destroy
    @rule = @project.rules.find(params[:id])
    @rule.destroy

    respond_to do |format|
      format.html { redirect_to project_rules_url(@project) }
      format.json { head :no_content }
    end
  end

  private 

  def find_project
    # p params
    @project = Project.find_by_identifier(params[:project_id])
    # p @project
  end          
end
