class CoursesController < ApplicationController
  Course
  Teacher
  Room
  before_filter :find_project, :authorize

  # GET /coursees
  # GET /coursees.json
  def index
    @courses = @project.courses.page(params[:page]).order('created_at desc')

    respond_to do |format|
      format.html # index.html.erb
      format.json { render json: @courses }
    end
  end

  # GET /coursees/1
  # GET /coursees/1.json
  def show
    @course = @project.courses.find(params[:id])
    gon.calendar_events = @course.calendar_events
    respond_to do |format|
      format.html # show.html.erb
      format.json { render json: @course }
    end
  end

  # GET /coursees/new
  # GET /coursees/new.json
  def new
    @course = @project.courses.new
    1.times do 
      @course.tasks.build
    end    
    respond_to do |format|
      format.html # new.html.erb
      format.json { render json: @course }
    end
  end

  # GET /coursees/1/edit
  def edit
    @course = @project.courses.find(params[:id])
  end

  # POST /coursees
  # POST /coursees.json
  def create
    @course = @project.courses.new(params[:course])

    if params[:rooms_id]
      @course.rooms = []
      params[:rooms_id].each do |room_id|
        @course.rooms << @project.rooms.find(room_id)
      end
    end
    if params[:teachers_id]
      @course.teachers = []
      params[:teachers_id].each do |teacher_id|
        @course.teachers << @project.teachers.find(teacher_id)
      end
    end

    respond_to do |format|
      if @course.save
        if params[:parents_id]
          @course.parents = []
          params[:parents_id].each do |parent_id|
            @course.parents << @project.courses.find(parent_id)
          end
        end
        format.html { redirect_to project_course_path(@project,@course), notice: 'course was successfully created.' }
        format.json { render json: @course, status: :created, location: @course }
      else
        format.html { render action: "new" }
        format.json { render json: @course.errors, status: :unprocessable_entity }
      end
    end
  end

  # PUT /coursees/1
  # PUT /coursees/1.json
  def update
    @course = @project.courses.find(params[:id])

    if params[:rooms_id]
      @course.rooms = []
      params[:rooms_id].each do |room_id|
        @course.rooms << @project.rooms.find(room_id)
      end
    end
    if params[:teachers_id]
      @course.teachers = []
      params[:teachers_id].each do |teacher_id|
        @course.teachers << @project.teachers.find(teacher_id)
      end
    end

    respond_to do |format|
      if @course.update_attributes(params[:course])
        if params[:parents_id]
          @course.parents = []
          params[:parents_id].each do |parent_id|
            @course.parents << @project.courses.find(parent_id)
          end
        end
        format.html { redirect_to project_course_path(@project,@course), notice: 'course was successfully updated.' }
        format.json { head :no_content }
      else
        format.html { render action: "edit" }
        format.json { render json: @course.errors, status: :unprocessable_entity }
      end
    end
  end

  # DELETE /coursees/1
  # DELETE /coursees/1.json
  def destroy
    @course = @project.courses.find(params[:id])
    @course.destroy

    respond_to do |format|
      format.html { redirect_to project_courses_path(@project) }
      format.json { head :no_content }
    end
  end

  private 

  def find_project
    # p params
    @project = Project.find_by_identifier(params[:project_id])
    # p @project
  end    
end
