class KlassplansController < ApplicationController
  Klassplan
  before_filter :find_project
  before_filter :authorize,:except=>[:ajax_menu,:ajax_students]

  # GET /klassplans
  # GET /klassplans.json
  def index
    Klassplanitem.auto_set_finished
    @klassplans = @project.klassplans.page(params[:page]).order('created_at desc')
    gon.calendar_events = Klassplanitem.calendar_events Klassplanitem.all
    respond_to do |format|
      format.html # index.html.erb
      format.json { render json: @klassplans }
    end
  end

  def planed_calendar
    @scope = 'planed'
    common_calendar
  end

  def planing_calendar
    @scope = 'planing'
    common_calendar
  end

  def autoplaning_calendar
    @scope = 'autoplaning'
    common_calendar
  end

  def finished_calendar
    @scope = 'finished'
    common_calendar
  end

  def all_calendar
    @scope = nil
    common_calendar
  end

  def common_calendar
    if @scope
      gon.calendar_events = Klassplanitem.calendar_events Klassplanitem.send(@scope)
    else
      gon.calendar_events = Klassplanitem.calendar_events Klassplanitem.all
    end
    render 'calendar_common'
  end

  def dashborad
    respond_to do |format|
      format.html # index.html.erb
      format.json { render json: @klassplans }
    end
  end


  # 确认排课
  def confirm_plan
    kp = @project.klassplans.find(params[:id])
    kp.confirm
    kp.baomings.each {|bm|bm.confirm}
    redirect_to project_klassplans_url(@project)
  end

  # 
  def start_plan
    @klassplan = @project.klassplans.find(params[:id])

    respond_to do |format|
      format.html # show.html.erb
      format.json { render json: @klassplan }
    end
  end

  # GET /klassplans/1
  # GET /klassplans/1.json
  def show
    @klassplan = @project.klassplans.find(params[:id])

    respond_to do |format|
      format.html # show.html.erb
      format.json { render json: @klassplan }
    end
  end

  # 新的创建排课接口
  def new2
    @klassplan = Klassplan.new
    @klassplan.klass = Klass.find_by_id(params[:klass_id])
    # 如果排课已经设置课程
    if @klassplan.klass
      1.upto(klass.section).each do |i|
        @klassplan.klassplanitems.build :number=>i
      end
    end
  end

  def create2

  end

  # GET /klassplans/new
  # GET /klassplans/new.json
  def new 
    gon.url_new = new_project_klassplan_path(@project)
    @klassplan = Klassplan.new
    @klassplan.klass = Klass.find_by_id(params[:klass_id])
    
    # 如果排课已经设置课程
    if @klassplan.klass
      @klassplan.klass_type = @klassplan.klass.class.to_s.downcase
      1.upto(@klassplan.klass.section).each do |i|
        kpi = @klassplan.klassplanitems.build :number=>i
        kpi.number = i
      end
    end
  end

  # GET /klassplans/1/edit
  def edit
    @klassplan = @project.klassplans.find(params[:id])
  end

  # POST /klassplans
  # POST /klassplans.json
  def create
    gon.url_new = new_project_klassplan_path(@project)
    @klassplan = @project.klassplans.new(params[:klassplan])
    respond_to do |format|
      if @klassplan.save
        @klassplan.plan
        Baoming.where(:id=>params[:baomings].select{|i|i[:checked]=='on'}.collect{|i|i[:id]}).each do |baoming|
          if baoming.payed? || baoming.used?
            baoming.apply @klassplan
          end
        end
        @klassplan.wait_teacher_confirm
        format.html { redirect_to project_klassplan_path(@project,@klassplan), notice: 'Klassplan was successfully created.' }
        format.json { render json: @klassplan, status: :created, location: @klassplan }
      else
        format.html { render action: "new" }
        format.json { render json: @klassplan.errors, status: :unprocessable_entity }
      end
    end
  end

  # PUT /klassplans/1
  # PUT /klassplans/1.json
  def update
    @klassplan = @project.klassplans.find(params[:id])

    respond_to do |format|
      if @klassplan.update_attributes(params[:klassplan])
        Baoming.where(:id=>params[:baomings].select{|i|i[:checked]=='on'}.collect{|i|i[:id]}).each do |baoming|
          baoming.apply @klassplan
        end
        format.js 
        format.html { redirect_to project_klassplan_path(@project,@klassplan), notice: 'Klassplan was successfully updated.' }
        format.json { head :no_content }
      else
        format.html { render action: "edit" }
        format.json { render json: @klassplan.errors, status: :unprocessable_entity }
      end
    end
  end

  # DELETE /klassplans/1
  # DELETE /klassplans/1.json
  def destroy
    @klassplan = @project.klassplans.find(params[:id])
    @klassplan.destroy

    redirect_to project_klassplans_path(@project)
  end

  # 动态得到菜单 
  def ajax_menu
    @category = params[:category]

    @klassplan = @project.klassplans.find(params[:id])
    @klassplan.category = @category
    @klassplan.save :validate=>false
    
    respond_to do |format|
      format.html # index.html.erb
      format.js #index.js.erb
    end
  end

  # 
  def ajax_students
    @klass = Klass.find(params[:id])
  end

  def ajax_kpis
    @klassplan = @project.klassplans.find(params[:id])
    @klassplan.klass_id = params[:klass_id]
    @klassplan.start_at = params[:start_at]
    @klassplan.save :validate=>false
    
    @klassplan.klassplanitems.destroy_all
    @klassplan.build_kpis @klassplan.start_at
    # @klassplan.build_kpis_simple
    respond_to do |format|
      format.html # index.html.erb
      format.js #index.js.erb
    end
  end

  def ajax_resources
    @klassplan = @project.klassplans.find(params[:id])
  end

  def ajax_teachers

  end

  def ajax_rooms

  end

  def ajax_baoming
    @klassplan = @project.klassplans.find(params[:id])
    bm = Baoming.find(params[:baoming_id])
    bm.apply @klassplan,params[:baoming].to_bool
    render :json=>{}
  end

  def wait_teacher_confirm
    @klassplan = @project.klassplans.find(params[:id])
    @klassplan.wait_teacher_confirm
    redirect_to project_klassplans_url(@project)
  end

  def auto
    gon.calendar_events = Klassplanitem.calendar_events Klassplanitem.autoplaning
  end

  def start_auto
    params[:klasses_id].each do |klass_id|
      Klassplan.start_auto Klass.find(klass_id)
    end

    respond_to do |format|
      format.html # index.html.erb
      format.js { render json: @klassplans }
    end

  end

  def wait_confirm
    @klassplan = @project.klassplans.find(params[:id])
    @klassplan.wait_confirm
    redirect_to project_klassplans_url(@project)    
  end
  private 

  def find_project
    # p params
    @project = Project.find_by_identifier(params[:project_id])
    # p @project
  end            
end
