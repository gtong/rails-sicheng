class TaskInstancesController < ApplicationController
  # GET /task_instances
  # GET /task_instances.json
  def index
    @task_instances = TaskInstance.order('created_at desc')

    respond_to do |format|
      format.html # index.html.erb
      format.json { render json: @task_instances }
    end
  end

  # GET /task_instances/1
  # GET /task_instances/1.json
  def show
    @task_instance = TaskInstance.find(params[:id])

    respond_to do |format|
      format.html # show.html.erb
      format.json { render json: @task_instance }
    end
  end

  # GET /task_instances/new
  # GET /task_instances/new.json
  def new
    @task_instance = TaskInstance.new

    respond_to do |format|
      format.html # new.html.erb
      format.json { render json: @task_instance }
    end
  end

  # GET /task_instances/1/edit
  def edit
    @task_instance = TaskInstance.find(params[:id])
  end

  # POST /task_instances
  # POST /task_instances.json
  def create
    @task_instance = TaskInstance.new(params[:task_instance])

    respond_to do |format|
      if @task_instance.save
        format.html { redirect_to @task_instance, notice: 'Task instance was successfully created.' }
        format.json { render json: @task_instance, status: :created, location: @task_instance }
      else
        format.html { render action: "new" }
        format.json { render json: @task_instance.errors, status: :unprocessable_entity }
      end
    end
  end

  # PUT /task_instances/1
  # PUT /task_instances/1.json
  def update
    @task_instance = TaskInstance.find(params[:id])

    respond_to do |format|
      if @task_instance.update_attributes(params[:task_instance])
        format.html { redirect_to @task_instance, notice: 'Task instance was successfully updated.' }
        format.json { head :no_content }
      else
        format.html { render action: "edit" }
        format.json { render json: @task_instance.errors, status: :unprocessable_entity }
      end
    end
  end

  # DELETE /task_instances/1
  # DELETE /task_instances/1.json
  def destroy
    @task_instance = TaskInstance.find(params[:id])
    @task_instance.destroy

    respond_to do |format|
      format.html { redirect_to task_instances_url }
      format.json { head :no_content }
    end
  end
end
