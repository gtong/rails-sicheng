class MarketsController < ApplicationController
  Market
  before_filter :find_project, :authorize

  # GET /markets
  # GET /markets.json
  def index
    @markets = @project.markets.page(params[:page]).order('created_at desc')
    gon.calendar_events = Market.calendar_events
    respond_to do |format|
      format.html # index.html.erb
      format.json { render json: @markets }
    end
  end

  # GET /markets/1
  # GET /markets/1.json
  def show
    @market = @project.markets.find(params[:id])

    respond_to do |format|
      format.html # show.html.erb
      format.json { render json: @market }
    end
  end

  # GET /markets/new
  # GET /markets/new.json
  def new
    @market = @project.markets.new

    respond_to do |format|
      format.html # new.html.erb
      format.json { render json: @market }
    end
  end

  # GET /markets/1/edit
  def edit
    @market = @project.markets.find(params[:id])
  end

  # POST /markets
  # POST /markets.json
  def create
    @market = @project.markets.new(params[:market])
    respond_to do |format|
      if @market.save
        @market.import
        format.html { redirect_to project_market_path(@market.project,@market), notice: 'Market was successfully created.' }
        format.json { render json: @market, status: :created, location: @market }
      else
        format.html { render action: "new" }
        format.json { render json: @market.errors, status: :unprocessable_entity }
      end
    end
  end

  # PUT /markets/1
  # PUT /markets/1.json
  def update
    @market = @project.markets.find(params[:id])
    respond_to do |format|
      if @market.update_attributes(params[:market])
        @market.import
        format.html { redirect_to project_market_path(@market.project,@market), notice: 'Market was successfully updated.' }
        format.json { head :no_content }
      else
        format.html { render action: "edit" }
        format.json { render json: @market.errors, status: :unprocessable_entity }
      end
    end
  end

  # DELETE /markets/1
  # DELETE /markets/1.json
  def destroy
    @market = @project.markets.find(params[:id])
    @market.destroy

    respond_to do |format|
      format.html { redirect_to project_markets_url(@project) }
      format.json { head :no_content }
    end
  end

  def import
    @market = @project.markets.find(params[:id])
  end

  def import_submit

  end

  private 

  def find_project
    # p params
    @project = Project.find_by_identifier(params[:project_id])
    # p @project
  end    
end
