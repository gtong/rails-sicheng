class AlumnasController < ApplicationController
  Student
  before_filter :find_project, :authorize  
  # GET /alumnas
  # GET /alumnas.json
  def index
    gon.new_notification_url = new_project_notification_path(@project)
    @alumnas = @project.students.alumna.order('created_at desc').filter(:params => params)
    respond_to do |format|
      format.html # index.html.erb
      format.json { render json: @alumnas }
    end
  end

  # GET /alumnas/1
  # GET /alumnas/1.json
  def show
    @alumna = @project.students.alumna.find(params[:id])
    @attachable = @alumna
    respond_to do |format|
      format.html # show.html.erb
      format.json { render json: @alumna }
    end
  end

  # GET /alumnas/new
  # GET /alumnas/new.json
  def new
    @alumna = @project.students.alumna.new

    respond_to do |format|
      format.html # new.html.erb
      format.json { render json: @alumna }
    end
  end

  # GET /alumnas/1/edit
  def edit
    @alumna = @project.students.alumna.find(params[:id])
    @attachable = @alumna
    if @alumna.attaches.image.blank?
      @alumna.attaches << Attach.image.build
    end 
    @alumna.attaches.image.first.state = 'image'
  end

  # POST /alumnas
  # POST /alumnas.json
  def create
    @alumna = @project.students.alumna.new(params[:student])

    respond_to do |format|
      if @alumna.save
        format.html { redirect_to project_alumna_path(@project,@alumna), notice: 'Student was successfully created.' }
        format.json { render json: @alumna, status: :created, location: @alumna }
      else
        format.html { render action: "new" }
        format.json { render json: @alumna.errors, status: :unprocessable_entity }
      end
    end
  end

  # PUT /alumnas/1
  # PUT /alumnas/1.json
  def update
    @alumna = @project.students.alumna.find(params[:id])

    respond_to do |format|
      if @alumna.update_attributes(params[:alumna])
        format.html { redirect_to @alumna, notice: 'alumna was successfully updated.' }
        format.json { head :no_content }
      else
        format.html { render action: "edit" }
        format.json { render json: @alumna.errors, status: :unprocessable_entity }
      end
    end
  end

  # DELETE /alumnas/1
  # DELETE /alumnas/1.json
  def destroy
    @alumna = @project.students.alumna.find(params[:id])
    @alumna.destroy

    respond_to do |format|
      format.html { redirect_to project_alumnas_url(@project) }
      format.json { head :no_content }
    end
  end

  def duplicate
    @alumna = @project.students.alumna.find(params[:id])
    @new = @alumna.dup
    @new.username = "#{@alumna.username} #{t :duplicate}"
    @new.save :validate=>false
    redirect_to request.referer
  end

  def destroy_multiple
    @project.students.alumna.destroy(params[:students])
    redirect_to request.referer
  end

  private 

  def find_project
    # p params
    @project = Project.find_by_identifier(params[:project_id])
    # p @project
  end          

  def set_state
    @state = :alumna
  end
end
