class EntranceController < ApplicationController
	before_filter :find_project

  def resources
    
  end

  def index
  	
  end

  def feedbacks
  end

  def finish_baoming_kpi
    bmkpi = BaomingsKlassplanitem.find(params[:id])
    bmkpi.finish
    redirect_to request.referer
  end

	private 

  def find_project
    # p params
    @project = Project.find_by_identifier(params[:project_id])
    # p @project
  end        
end
