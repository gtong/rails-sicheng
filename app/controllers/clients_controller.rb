class ClientsController < ApplicationController
  Student
  Market
  before_filter :find_project, :authorize  
  # GET /clients
  # GET /clients.json
  def index
    gon.new_notification_url = new_project_notification_path(@project)
    @clients = @project.students.client.order('created_at desc').filter(:params => params)
    respond_to do |format|
      format.html # index.html.erb
      format.json { render json: @clients }
    end
  end

  # GET /clients/1
  # GET /clients/1.json
  def show
    @client = @project.students.client.find(params[:id])
    @attachable = @client
    respond_to do |format|
      format.html # show.html.erb
      format.json { render json: @client }
    end
  end

  # GET /clients/new
  # GET /clients/new.json
  def new
    @client = @project.students.client.new
    gon.schools =  Person.group('school').select('school').map {|i|{:id=>i.id,:name=>i.school}}.reject {|i|i[:name].nil? || i[:name]==''}
    gon.colleges =  Person.group('college').select('college').map {|i|{:id=>i.id,:name=>i.college}}.reject {|i|i[:name].nil? || i[:name]==''}
    gon.majors =  Person.group('major').select('major').map {|i|{:id=>i.id,:name=>i.major}}.reject {|i|i[:name].nil? || i[:name]==''}
    @client.attaches.image.build
    @attachable = @client
    @client.markets << @project.markets.find(params[:market_id]) if params[:market_id]
    respond_to do |format|
      format.html # new.html.erb
      format.json { render json: @client }
    end
  end

  # GET /clients/1/edit
  def edit
    @client = @project.students.client.find(params[:id])
    @attachable = @client
    if @client.attaches.image.blank?
      @client.attaches << Attach.image.build
    end 
    @client.attaches.image.first.state = 'image'
  end

  # POST /clients
  # POST /clients.json
  def create
    @client = @project.students.client.new(params[:student])
    @attachable = @client
    respond_to do |format|
      if @client.save
        format.html { redirect_to project_client_path(@project,@client), notice: 'Student was successfully created.' }
        format.json { render json: @client, status: :created, location: @client }
      else
        format.html { render action: "new" }
        format.json { render json: @client.errors, status: :unprocessable_entity }
      end
    end
  end

  # PUT /clients/1
  # PUT /clients/1.json
  def update
    @client = @project.students.client.find(params[:id])
    @attachable = @client
    respond_to do |format|
      if @client.update_attributes(params[:client])
        format.html { redirect_to project_client_path(@project,@client), notice: 'Client was successfully updated.' }
        format.json { head :no_content }
      else
        format.html { render action: "edit" }
        format.json { render json: @client.errors, status: :unprocessable_entity }
      end
    end
  end

  # DELETE /clients/1
  # DELETE /clients/1.json
  def destroy
    @client = @project.students.client.find(params[:id])
    @client.destroy

    respond_to do |format|
      format.html { redirect_to project_clients_url(@project) }
      format.json { head :no_content }
    end
  end

  def duplicate
    @client = @project.students.client.find(params[:id])
    @new = @client.dup
    @new.username = "#{@client.username} #{t :duplicate}"
    @new.save :validate=>false
    redirect_to request.referer
  end

  def destroy_multiple
    @project.students.client.destroy(params[:students])
    redirect_to request.referer
  end

  private 

  def find_project
    # p params
    @project = Project.find_by_identifier(params[:project_id])
    # p @project
  end          

  def set_state
    @state = :client
  end
end
