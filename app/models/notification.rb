# encoding: utf-8
class Notification < ActiveRecord::Base
  # paginates_per 10
  attr_accessible :required,:content, :notificatable_id, :notificatable_type, :state, :title,:notify_at,:notificatable,:person,:person_id
  
  belongs_to :notificatable,:polymorphic=>true

  belongs_to :person

  before_validation do |record|
    record.state = 'normal' if record.state == '无需确认通知'
    record.state = 'confirm' if record.state == '需确认通知'
  end

  before_create :set_notify_at

  # 创建待老师确认的通知 ，notificate is klassplan
  def self.create_teacher_confirm(notificatable)
    n = Notification.new
    n.state = 'teacher_confirm'
    n.notificatable = notificatable
    n.person = notificatable.teacher
    n.title = n.notification_title
    n.content = n.notification_content
    # p n.title
    n.save
  end

  # 创建待学生确认的通知 ，notificate is baoming
  def self.create_student_confirm(notificatable,student)
    n = Notification.new
    n.state = 'student_confirm'
    n.notificatable = notificatable
    n.person = student
    n.title = n.notification_title
    n.content = n.notification_content
    n.valid?
    n.save
  end

  # 设置通知时间
  def set_notify_at
    self.notify_at = Time.zone.now if self.notify_at.blank?
  end


  def handle_response response
    if response == 'confirm'
      self.confirm
    else
      self.unconfirm
    end
  end

  # 根据notificatable 获得 通知标题
  def notification_title
  	case state 
    when 'teacher_confirm'
			notificatable.teacher_confirm_title
    # when 'student_confirm'
    #   notificatable.student_confirm_title      
    else
      notificatable.confirm_title
		end
	end

  # 根据notificatable 获得 通知内容
	def notification_content
		case state 
    when 'teacher_confirm'
			notificatable.teacher_confirm_content
		# when 'student_confirm'
		# 	notificatable.student_confirm_content
    else 
      notificatable.confirm_content
		end
	end

  # 确认通知
	def confirm
    if notificatable
      case self.state 
      when 'teacher_confirm'
        notificatable.wait_student_confirm 
      # when 'student_confirm'
      #   notificatable.wait_student_confirm 
      else
        notificatable.confirm
      end
    end
    
		self.response = :confirm
		self.save
	end		

  # 通知来源 ， 服务器还是客户端
  def laiyuan
    if response == nil
      ''
    elsif is_server
      '服务器'
    else
      '客户端'
    end
  end

	def unconfirm
    begin
		  notificatable.unconfirm if notificatable 
    rescue
    end
		self.response = :unconfirm
		self.save
	end

  def clear_confirm
    self.response = nil
    self.save
  end		

  state_machine :state do 
    state :normal do
      def state_zh
        '无需确认通知'
      end
    end

    state :confirm do
      def state_zh
        '需确认通知'
      end
    end

  	state :teacher_confirm do
      def state_zh
        '教师确认通知'
      end
  	end

  	state :student_confirm do
      def state_zh
        '学员确认通知'
      end
  	end
  end 
end