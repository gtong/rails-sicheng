# encoding: utf-8
require 'roo'
class Market < ActiveRecord::Base
  attr_accessible :title

  attr_accessible :attachment
  has_attached_file :attachment

  has_many :markets_students
  has_many :students,:through=>:markets_students
  belongs_to :project
  # has_and_belongs_to_many :people,:uniq=>true

  validates :title, :presence => true  
  validates_attachment_content_type :attachment,:content_type=>["application/vnd.ms-excel",     
             "application/vnd.openxmlformats-officedocument.spreadsheetml.sheet",]

  attr_accessible :leixing
  attr_accessible :state
  attr_accessible :hezuofang
  attr_accessible :zhuti
  attr_accessible :xingshi
  attr_accessible :qtsj
  attr_accessible :yjgm
  attr_accessible :openclass  

  attr_accessible :zxqk
  attr_accessible :dcrs
  attr_accessible :hdxc  
  attr_accessible :hdgm  
  attr_accessible :xcxg
  attr_accessible :wjdcjg

  attr_accessible :hdht
  attr_accessible :hdzp
  attr_accessible :cg
  attr_accessible :zj
  attr_accessible :zzfk

  AttrGroups = [
    [:openclass,:leixing,:state,:hezuofang,:zhuti,:xingshi,:qtsj,:yjgm],
    [:zxqk,:dcrs,:hdgm,:hdxc,:xcxg,:wjdcjg],
    [:hdht,:hdzp,:cg,:zj,:zzfk ]
  ]

  scope :openclass, :conditions => { :openclass=>true }
  def import
    begin
      s = Roo::Excelx.new(attachment.path)
      self.students = []
      s.to_a.each do |data|
        student  = Student.find_or_create_by_phone(data[0])
        student.username = data[1]
        student.school = data[2]
        student.college = data[3]
        student.major = data[4]
        student.grade = data[5]
        student.signup_openclass = data[6]
        student.project_id = self.project.id
        if data[7] 
          if data[7] == '客户'
            student.state = 'client'
          elsif data[7] == '学员'
            student.state = 'student'
          elsif data[7] == '校友'
            student.state = 'alumna'
          end
        end
        student.save :validate=>false
        student.markets << self
        ms = MarketsStudent.where(:market_id=>self.id,:student_id=>student.id).first
        ms.state = data[7]
        ms.save
      end
    rescue
    end  
  end

  module BelongsToRelation
    def self.included(base)
      base.class_eval do
        has_many :markets
      end
    end
  end


  def self.calendar_events
    i = 1
    all.collect do |r|
      i = i+1
      {
        :title=>r.title,
        :start=>r.start_at,
        :end=>r.end_at,
        :allDay=>false,
        :color=>"rgb(#{rand(255)},#{rand(255)},#{rand(255)})"
      }
    end
  end  

# 1. 单次市场活动的转化率a1：本次客户数/活动规模*100%
  def market_rate
    hdgm == 0 ? 0 : students.count/hdgm
  end
# 2. 单次公开课的转化率 b1：本次公开课上报名测评的人数 / 参加公开课的总人数*100%（公开课现在放在哪个模块里？）
  def openclass_rate
    a1,a2 = 0,0
    a2 = students.count
    students.each do |s|
      if s.baomings.count >0 
        a1 = a1 +1
      end
    end
    a2 == 0 ? 0 : a1*1.0/a2    
  end
end

Project.send(:include,Market::BelongsToRelation)
