class KlassplanitemsStudent < ActiveRecord::Base
  attr_accessible :klassplanitem_id, :state, :student_id
  belongs_to :klassplanitem
  belongs_to :student
end
