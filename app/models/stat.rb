class Stat
	# 1. 所有活动里的报名公开课的总人数 A1、所有活动总到场人数 A2、市场活动的平均转化率a2=A1/A2*100%（时间戳=活动现场时间（活动现场本身就是一个人工填写的字段））
	def self.market_to_openclass_rate(from=DateTime.now-1000.year,to=DateTime.now)
		a1,a2 = 0,0
		markets = Market.where(:hdxc=>from..to)
		markets.each do |m|
			a2 = a2 + m.students.count
			m.students.each do |s|
				a1 = a1 +1 if (s.markets.openclass.count > 0)
			end
		end
		a2 == 0 ? 0 : a1*1.0/a2
	end
	# 2. 同上：所有公开课里，最终报名测评的总人数B1、参加公开课的总人数 B2、公开课的平均转化率 b2=B1/B2*100%（时间戳同上）【注：可以把公开课作为一种市场活动处理，但是需要和其他活动分开统计】
	def self.openclass_to_review_rate(from=DateTime.now-1000.year,to=DateTime.now)
		a1,a2 = 0,0
		markets = Market.openclass.where(:hdxc=>from..to)
		markets.each do |m|
			a2 = a2 + m.students.count
			m.students.each do |s|
				a1 = a1 +1 if (s.reviews.count > 0)
			end
		end
		a2 == 0 ? 0 : a1*1.0/a2
	end
	# 3. 测评后报名率 c1=成功报名的总人数C1 / 参加测评的总人数C2（前者的时间戳=该名学生的付款时间，即报名时间；后者的时间戳=该测评服务的约谈时间，人工填写字段）

	def self.review_to_baoming_rate(from=DateTime.now-1000.year,to=DateTime.now)
		a1,a2 = 0,0
		reviews = Review.where(:start_at=>from..to)
		a2 = reviews.count
		reviews.each do |r|
			a1 = a1 +1 if (r.student.baomings.payed.count > 0 || r.student.baomings.used.count > 0)
		end
		a2 == 0 ? 0 : a1*1.0/a2
	end
	 # 所有活动里的客户最终通过测评的全程转化率 d1=成功报名的总人数C1 / 所有活动总到场人数 A2
	def self.market_to_baoming_rate
		a1,a2 = 0,0
		markets = Market.where(true)
		markets.each do |m|
			a2 = a2 + m.students.count
			m.students.each do |s|
				a1 = a1 +1 if (s.baomings.payed.count > 0 || s.baomings.used.count > 0)
			end
		end
		a2 == 0 ? 0 : a1*1.0/a2
	end
	# 平均总客单价
	def self.student_fee_avg
		money = 0
		students = Student.student
		students.each do |s|
			s.baomings.each do |b|
				if b.payed? || b.used?
					money = money + b.price
				end
			end
		end
		(money*1.0/students.count).round(2)
	end
	# 平均首次报名金额
	def self.student_first_fee_avg
		money = 0
		students = Student.student
		students.each do |s|
			s.baomings.group('created_at').each do |i|
				if i.payed? || i.used?
					money = money + s.baomings.where(:created_at=>i.created_at).inject(0){|sum,item|sum+item.price}
				end
			end
		end
		(money*1.0/students.count).round(2)
	end
	# 平均补报总金额
	def self.student_other_fee_avg
		money = 0
		students = Student.student
		students.each do |s|
			s.baomings.group('created_at').each do |i|
				if i.payed? || i.used?
					money = money + s.baomings.where("created_at != ?",i.created_at).inject(0){|sum,item|sum+item.price}
				end
			end
		end
		(money*1.0/students.count).round(2)
	end
end