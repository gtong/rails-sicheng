class Choice < ActiveRecord::Base
  attr_accessible :answer, :question_id, :title
  belongs_to :question
  validates :title,:presence=>true
end
