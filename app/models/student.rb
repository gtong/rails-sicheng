# encoding: utf-8
class Student < Person

  validate :validate_info
  def validate_info
    if self.phone.blank? && self.qq.blank? && self.email.blank? && self.weixin.blank?
      errors.add(:phone,'电话,qq,微信,邮箱至少添一项');
      errors.add(:qq,'电话,qq,微信,邮箱至少添一项');
      errors.add(:email,'电话,qq,微信,邮箱至少添一项');
      errors.add(:weixin,'电话,qq,微信,邮箱至少添一项');
      
    end
  end
  validates :phone,:uniqueness=>true,:allow_nil=>true,:unless=>'phone.nil?'
  validates :phone,:numericality=>true,:allow_nil=>true,:unless=>'phone.nil?'
  # validates :qq, uniqueness: true,:allow_blank => true
  # validates :weixin, uniqueness: true,:allow_blank => true
  # validates :email, uniqueness: true,:allow_blank => true
  # attr_accessible :project_id
  has_many :markets_students,:uniq=>true
  has_many :markets,:through=>:markets_students    ,:uniq=>true
  has_many :baomings,:uniq=>true  
  has_many :task_instances,:uniq=>true
  has_many :baomings_klassplanitems,:through=>:baomings,:uniq=>true
  has_many :klassplans,:through=>:baomings ,:uniq=>true
  has_many :feedbacks,:uniq=>true
  has_many :candidates,:uniq=>true
  has_many :scores,:uniq=>true
  has_many :reviews,:uniq=>true
  has_many :klasses,:through=>:baomings do 
    def payed
      where("baomings.state = ?",:payed)
    end

    def unpayed
      where("baomings.state = ?",:unpayed)
    end
  end

  has_many :klassplanitems,:through=>:baomings_klassplanitems

  Grade = (1..6).to_a

  module BelongsToRelation
    def self.included(base)
      base.class_eval do
        has_many :students
      end
    end
  end

  def calendar_events start_at=DateTime.new(2000,1,1),end_at=DateTime.new(3000,1,1)
    kpis = klassplanitems.where(" klassplanitems.start_at>= ? and klassplanitems.end_at <= ?",start_at,end_at)
    Klassplanitem.calendar_events(kpis)
  end

  state_machine :state,:initial=>:client do
    state :client do
      def redirect_link()
        redirect_to project_client_path(self.project,self), notice: '客户创建成功'
      end
    end

    state :student do
      def redirect_link()
        redirect_to project_student_path(self.project,self), notice: '学员创建成功'
      end
    end

    state :alumna do
      def redirect_link()
        redirect_to project_alumna_path(self.project,self), notice: '校友创建成功'
      end
    end

    event :to_client do
      transition any=>:client
    end

    event :to_student do
      transition any=>:student
    end

    event :to_alumna do
      transition any=>:alumna
    end
  end

  state_machine.states.map do |state|
    scope state.name, :conditions => { :state => state.name.to_s }
  end  

  def duplicate
    dup = self.dup
    dup.username = "#{username} 复制"
    dup.qq = nil
    dup.weixin = nil
    dup.email = nil
    dup.phone = nil
    dup.save :validate=>false
  end
end

Project.send(:include,Student::BelongsToRelation)