<?php

require_once ( "../Channel.class.php" ) ;
require_once ( "../lib/r.php" ) ;

R::setup('mysql:host=localhost;dbname=redmine_sicheng_development','root','');

//请开发者设置自己的apiKey与secretKey
$apiKey = "borbqEhE3qbYzX9944PMDRiV";
$secretKey = "KmxLY1UEKiRvGdc2oS0pHXd2BO5Lz5ue";

function error_output ( $str ) 
{
	echo "\033[1;40;31m" . $str ."\033[0m" . "\n";
}

function right_output ( $str ) 
{
    echo "\033[1;40;32m" . $str ."\033[0m" . "\n";
}

	//推送android设备消息
function test_pushMessage_android ($title,$tag,$message_key,$notification_id)
{
  global $apiKey;
	global $secretKey;
  $channel = new Channel ( $apiKey, $secretKey ) ;
	//推送消息到某个user，设置push_type = 1; 
	//推送消息到一个tag中的全部user，设置push_type = 2;
	//推送消息到该app中的全部user，设置push_type = 3;
	$push_type = 3; //推送单播消息
	// $optional[Channel::USER_ID] = $user_id; //如果推送单播消息，需要指定user
	//optional[Channel::TAG_NAME] = "xxxx";  //如果推送tag消息，需要指定tag_name

	//指定发到android设备
	$optional[Channel::DEVICE_TYPE] = 3;
	//指定消息类型为通知
	$optional[Channel::MESSAGE_TYPE] = 1;
	//通知类型的内容必须按指定内容发送，示例如下：
	$message = array(
		'title'=>$title,
		'tag'=>$tag,
		"description"=> "open url",
		"notification_basic_style"=>7,
		"open_type"=>1,
	);
	$message = json_encode($message);
	right_output($message);
	$message_key = $message_key;
    $ret = $channel->pushMessage ( $push_type, $message, $message_key, $optional ) ;
    if ( false === $ret )
    {
        error_output ( 'WRONG, ' . __FUNCTION__ . ' ERROR!!!!!' ) ;
        error_output ( 'ERROR NUMBER: ' . $channel->errno ( ) ) ;
        error_output ( 'ERROR MESSAGE: ' . $channel->errmsg ( ) ) ;
        error_output ( 'REQUEST ID: ' . $channel->getRequestId ( ) );
    }
    else
    {
    	// /* 上午8:40:31 root@127.0.0.1 */ UPDATE `notifications` SET `android_pushed` = '0' WHERE `id` = '44';
    		R::exec("UPDATE `notifications` SET `android_pushed` = '1' WHERE `id` = '".$notification_id."';");
    		R::exec("UPDATE `notifications` SET `android_request_id` = '".$ret['request_id']."' WHERE `id` = '".$notification_id."';");
        right_output ( 'SUCC, ' . __FUNCTION__ . ' OK!!!!!' ) ;
        right_output ( 'result: ' . print_r ( $ret, true ) ) ;
    }
}

//推送ios设备消息
function test_pushMessage_ios ($title,$tag,$message_key,$notification_id)
{
  global $apiKey;
	global $secretKey;
  $channel = new Channel ( $apiKey, $secretKey ) ;

	$push_type = 3; //推送单播消息
	// $optional[Channel::USER_ID] = $user_id; //如果推送单播消息，需要指定user

	//指定发到ios设备
	$optional[Channel::DEVICE_TYPE] = 4;
	//指定消息类型为通知
	$optional[Channel::MESSAGE_TYPE] = 1;
	//如果ios应用当前部署状态为开发状态，指定DEPLOY_STATUS为1，默认是生产状态，值为2.
	//旧版本曾采用不同的域名区分部署状态，仍然支持。
	// $optional[Channel::DEPLOY_STATUS] = 1;
	//通知类型的内容必须按指定内容发送，示例如下：
	$message = array(
		'tag'=>$tag,
		'aps'=>array(
			'alert'=>$title,
			'sound'=>'',
			'badge'=>0
		)
	);
	
	$message = json_encode($message);
	$message_key = $message_key;
    $ret = $channel->pushMessage ( $push_type, $message, $message_key, $optional ) ;
    if ( false === $ret )
    {
        error_output ( 'WRONG, ' . __FUNCTION__ . ' ERROR!!!!!' ) ;
        error_output ( 'ERROR NUMBER: ' . $channel->errno ( ) ) ;
        error_output ( 'ERROR MESSAGE: ' . $channel->errmsg ( ) ) ;
        error_output ( 'REQUEST ID: ' . $channel->getRequestId ( ) );
    }
    else
    {
    		R::exec("UPDATE `notifications` SET `ios_pushed` = '1' WHERE `id` = '".$notification_id."';");
    		R::exec("UPDATE `notifications` SET `ios_request_id` = '".$ret['request_id']."' WHERE `id` = '".$notification_id."';");
        right_output ( 'SUCC, ' . __FUNCTION__ . ' OK!!!!!' ) ;
        right_output ( 'result: ' . print_r ( $ret, true ) ) ;
    }
}



function send_messages(){
	
	$result = R::getAll( "SELECT * FROM `notifications` WHERE `android_pushed` = '0'", 
        array(':title'=>'home') 
  );
  foreach ($result as $key => $item) {
  	var_dump($item);
  	test_pushMessage_android($item['title'],'person_'.$item['person_id'],'notification'.$item['id'],$item['id']);
  }

	$result = R::getAll( "SELECT * FROM `notifications` WHERE `ios_pushed` = '0'", 
        array(':title'=>'home') 
  );
  foreach ($result as $key => $item) {
  	test_pushMessage_ios($item['title'],'person_'.$item['person_id'],'notification'.$item['id'],$item['id']);
  }
}

send_messages();
?>