# encoding: utf-8
class Klassplanitem < ActiveRecord::Base
  attr_accessible :end_at, :klass_id, :room_id, :start_at, :teacher_id,:state,:number

  belongs_to :klassplan
  belongs_to :teacher
  belongs_to :room
	belongs_to :klass
  has_many :baomings_klassplanitems
  has_many :baomings,:through=>:baomings_klassplanitems

  validates :start_at,:end_at,:room_id,:teacher_id, :presence => true

  validate :start_at_end_at_must_be_same_day,:start_at_must_be_less_than_end_at

  # validate :start_at_must_be_larger_than_previous_end_at
  def start_at_must_be_larger_than_previous_end_at

    last = self.klassplan.klassplanitems.where(:number=>self.number)
    return if start_at.nil? || last.end_at.nil?
    if last && last.end_at < start_at
      errors.add(:start_at,:start_at_must_be_larger_than_previous_end_at)
    end  
  end

  def start_at_must_be_less_than_end_at
    return if start_at.nil? || end_at.nil?
    errors.add(:end_at,:start_at_must_be_less_than_end_at) unless start_at < end_at
  end

  def start_at_end_at_must_be_same_day
    return if start_at.nil? || end_at.nil?
    errors.add(:end_at,:start_at_end_at_must_be_same_day) if start_at.to_date != end_at.to_date
  end


  state_machine :state,:initial=>:planing do

    state :planing do
      def state_zh
        '排课中'
      end
    end
    
    state :planed do
      def state_zh
        '已排课'
      end
    end

    state :finished do
      def state_zh
        '已完成'
      end
    end

    event :plan do
      transition :unplaned=>:planing
    end

    event :confirm do
      transition [:autoplaning,:planing]=>:planed
    end

    event :finish do
      transition :planed=>:finished
    end

  end

  state_machine.states.map do |state|
    scope state.name, :conditions => { :state => state.name.to_s }
  end

	def self.calendar_events kpis
    colors = {}
    begin
      color = kpis.first.klassplan.klass.color
    rescue
      color = nil
    end
    if color.nil?
      color = Klass::Colors.sample
    end
    kpis.collect do |kpi|
      _index = kpi.klassplan.klassplanitems.index(kpi)
      colors[kpi.klassplan] = colors[kpi.klassplan] || "rgb(#{rand(255)},#{rand(255)},#{rand(255)})"
      
      
      {
        :title=>"#{kpi.klassplan.klass.title} #{kpi.teacher.username} #{_index+1}",
        :start=>kpi.start_at,
        :end=>kpi.end_at,
        :color=>color,
        :allDay=>false
      }
    end
  end  

  def self.auto_set_finished
    where(true).where(:finished=>nil).each {|i|i.auto_set_finished}
  end

  # 设置是否已经上课
  def auto_set_finished
    if self.finished.nil? and DateTime.now > self.end_at
      self.finished = true
      self.save :validate=>false
    end
  end
end
