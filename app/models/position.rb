class Position < ActiveRecord::Base
  attr_accessible :description, :title
  belongs_to :person
  validates :title,:person_id,:presence=>true
end
