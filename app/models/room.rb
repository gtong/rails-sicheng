class Room < ActiveRecord::Base
  include AvailableRange
  attr_accessible :mic, :projector, :sound, :title, :laser, :network

  belongs_to :project

  has_and_belongs_to_many :klasses,:uniq=>true
  has_many :klassplans

  has_many :klassplanitems

  has_and_belongs_to_many :courses,:uniq=>true
  has_many :courseplans

  has_and_belongs_to_many :services,:uniq=>true
  has_many :serviceplans

  validates :title,:presence=>true

  def available_for_klass(klass)
  	if klass.projector == true && self.projector == false
  		return false
  	end

  	if klass.sound == true && self.sound == false
  		return false
  	end

  	if klass.mic == true && self.mic == false
  		return false
  	end

  	return true
  end

  def calendar_events start_at=DateTime.new(2000,1,1),end_at=DateTime.new(3000,1,1)
    kpis = Klassplanitem.where("room_id = ? and start_at>= ? and end_at <= ?",self.id,start_at,end_at)
    Klassplanitem.calendar_events(kpis)
  end

  module BelongsToRelation
    def self.included(base)
      base.class_eval do
        has_many :rooms
      end
    end
  end
end

Project.send(:include,Room::BelongsToRelation)