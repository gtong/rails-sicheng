# encoding: utf-8
class Service < Klass
	attr_accessible :title,:menu,:capacity,:finished
  # Menu = ['VIP 服务类','一对一咨询服务类']
  Menu = ['储备 VIP 类','应聘 VIP 类','一对一咨询服务类']
	# Menu = %w(个性化服务 储备VIP 应聘VIP)
	Capacity = (1..5).to_a
	validates :capacity,:numericality => { :less_than_or_equal_to => 5,:greater_than=>0 }
  validates :menu,:inclusion=>{:in=>Menu}


  module BelongsToRelation
    def self.included(base)
      base.class_eval do
        has_many :services
      end
    end
  end

end

Project.send(:include,Service::BelongsToRelation)