# encoding: utf-8
class Baoming < ActiveRecord::Base
  attr_accessible :discount_way, :important, :installment, :installment_duration, :installment_last, :installment_value, :makeup, :next, :payment_way, :person_id, :end_at,:state,:free,:price,:payed_at
  belongs_to :student
  belongs_to :project
  belongs_to :review
  belongs_to :klass

  has_and_belongs_to_many :klassplans
  has_many :baomings_klassplans
  has_many :baomings_klassplanitems

  DiscountWay = %w(无折扣 95折 9折 85折 8折 75折 7折 65折 6折 55折 5折)
  PaymentWay = %w(储蓄卡 信用卡 转账 现金)
  Important = %w(交钱多 水平好 影响力大 重点市场)
  State = %w(unpayed payed)
  # 0：不需要主动约；1：提醒应聘VIP/SVIP（应聘季暑假前、暑假后）；2：提醒应聘课程（应聘季暑假前、暑假后）；3、提醒储备VIP/SVIP（学期末、学期初）；4、按学期报（学期末学期初）；5、按课程报（最后一门课程结束前1周）
  Makeup = (0..5).to_a
  Will = %w(待报名 强烈 待影响 不合适)

  validates :price, :numericality => {:greater_than_or_equal_to => 0, :allow_nil => false}

  before_validation :set_price_from_klass


# 当报名价钱为空时 ， 根据课程设置价钱.
  def set_price_from_klass
    if self.price.blank? & !self.klass.blank?
      self.price = self.klass.price 
    end
  end

  state_machine :state,:initial=>'unpayed' do 
    # 能够付款

    state 'unpayed' do
      def state_zh
        '未付款'
      end
      def klassplanable?
        '未付款'
      end
    end

    # 排课之后需要把状态设置为used ， 否则排课时，还会在待排课中显示    
    state 'payed' do
      def state_zh
        '已付款'
      end
      def klassplanable?
        # return '无排课计划' unless klassplans.blank?
        true
      end
    end

    state 'used' do
      def state_zh
        '已学习'
      end
      def klassplanable?
        '已使用'
      end
    end

    # 将报名变为未付款的状态，
    # event 'unpay' do
    #   transition 'payed'=>'unpayed'
    # end

    # 付款
    event 'pay' do
      transition 'unpayed'=>'payed'
    end

    # 标示这个已付款科目已使用
    event 'use' do
      transition 'payed'=>'used'
    end   

    before_transition 'unpayed'=>'payed' do |instance,transition|
      # 付款时更新报名时间 ，应该是付款时间 , 实际走得是表单
      instance.payed_at = Time.now unless instance.payed_at
    end
  
    after_transition 'unpayed'=>'payed' do |instance,transition|
      # 付款之后变为学员 , 实际过程走的是表单 ， 可以改成这里实现
      instance.student.to_student
      # 付款后添加积分
      instance.student.scores.create :title=>"报名课程 #{instance.klass.title}",:value=>instance.klass.price/1000 * Setting.baoming_score
    end    

    before_transition any=>'unpayed' do |instance,transition|
      instance.payed_at = nil
    end 
  end

  # 使用after_save 进行判断
  before_save do |record|
    if record.changed_attributes['state'] == 'unpayed' && record.state = 'payed'
      # 付款时更新报名时间 ，应该是付款时间 ,
      record.payed_at = Time.now 
    end
  end

  after_save do |record|
    if record.changed_attributes['state'] == 'unpayed' && record.state = 'payed'
      # 付款之后变为学员
      record.student.to_student
      # 付款后添加积分
      record.student.scores.create :title=>"报名课程 #{record.klass.title}",:value=>record.klass.price/1000 * Setting.baoming_score.to_i
    end
  end

  state_machine.states.map do |state|
    scope state.name, :conditions => { :state => state.name.to_s }
  end

  module BelongsToRelation
    def self.included(base)
      base.class_eval do
        has_many :baomings
      end
    end
  end

  validates :student,:klass,:presence=>true

  validates :makeup,:inclusion=>{:in=>Makeup},:if=>"state== 'payed'"

  validates :important,:inclusion=>{:in=>Important},:if=>"state== 'payed'"

  validates :end_at,:presence=>true,:if=>"state== 'payed' && klass && klass.kind == 'A'"  

  validates :discount_way, :inclusion=>{:in=>DiscountWay},:if=>" state== 'payed' && free == false "

  validates :payment_way, :inclusion=>{:in=>PaymentWay},:if=>" state== 'payed' && free == false"

  validates :installment_value,:installment_last,:installment_duration, :numericality => { :greater_than => 0 },:if=>'installment == true'

  after_save :convert_client_to_student

  # 付款之后变为学员 , 实际过程走的是表单 ， 可以改成这里实现
  def convert_client_to_student
    if self.student.client? && self.payed?
      self.student.to_student
    end  
  end

  def convert_student_to_alumna
    # self.student.to_alumna
  end


  def set_klassplan klassplan
    klassplans << klassplan
    plan
  end

  # 与klassplan建立关系，生成 baomings_klassplanitems 数据
  # bm.state = planing bm_kpis = planing
  
  def apply klassplan,exe=true
    if exe
      # 增加自动判断 baomings_klassplans 类型的处理
      if baomings_klassplans.count != 0
      # 如果改报名已被排课过 ， 则一定是补课        
        klassplan_type = 'buke'
      elsif klassplan.planing?
      # 如果排课未完成 ，则是普通排课
        klassplan_type = 'paike'        
      else
      # 如果排课已完成，则是插班
        klassplan_type = 'chaban'
      end
      # 创建学员排课
      bm_kp = self.baomings_klassplans.find_or_initialize_by_klassplan_id klassplan.id
      bm_kp.klassplan_type = klassplan_type
      bm_kp.save
      # 学员排课
      bm_kp.plan
      self.use
    else
      bm_kp = self.baomings_klassplans.find_klassplan_id klassplan
      bm_kp.unplan if bm_kp
    end
  end
end


Project.send(:include,Baoming::BelongsToRelation)