# encoding: utf-8
class Review < ActiveRecord::Base
  attr_accessible :person_id, :start_at, :title, :will,:end_at,:report_hard,:report_soft,:student_id,:day,:datetime_range
  attr_accessible :questions_attributes
  attr_accessor :datetime_range
  Will = %w(待报名 强烈 待影响 不合适)
  belongs_to :user
  belongs_to :student
  belongs_to :project
  has_many :baomings
  has_many :klasses,:through=>:baomings
  has_many :questions,:uniq=>true,:dependent=>:destroy
  validates :student,:presence=>true
  validates :title,:presence=>true
  validates :start_at,:presence=>true
  validates :day,:presence=>true
  validates :end_at,:presence=>true
  validates :attachment,:presence=>true

  validate :validate_duration

  scope :by_project , lambda {|project| where(:project_id => project.id)}

  attr_accessible :attachment
  has_attached_file :attachment

  accepts_nested_attributes_for :questions, :allow_destroy => true

  state_machine :state,:initial=>:unfinished do 
    state :unfinished do
      def event_color
        '#d43f3a'
      end
    end

    state :finished do
      def event_color
        '#4cae4c'
      end
    end

    # state :feedbacked do
    #   def event_color
    #     '#4cae4c'
    #   end
    # end

    state :payed do

    end

    event :finish do
      transition [:unfinished] => :finished
    end

    # event :feedback do
    #   transition [:unfinished] => :finished
    # end

    event :pay do
      transition [:finished] => :payed
    end
  end

  state_machine.states.map do |state|
    scope state.name, :conditions => { :state => state.name.to_s }
  end

  def self.calendar_events
    all.collect do |r|
      {
        :title=>"#{r.user.name} 评测 #{r.student.try(:username)}",
        :start=>r.start_at,
        :end=>r.end_at,
        :allDay=>false,
        :color=>r.event_color
      }
    end
  end

  def import
    if attachment
      begin
        s = Roo::Excelx.new(attachment.path)
      rescue Exception=>ex
        s = Roo::Excel.new(attachment.path)
      end
      self.questions.each {|q|q.destroy}
      s.to_a[1..-1].each do |data|
        question = self.questions.create(
          :page=>data[0].to_i.to_s,
          :title=>data[2],
          :state=>data[1],
          :description=>data[3]
        )
        data[4..-1].each do |c|
          question.choices << Choice.new(:title=>c)
        end
        question.valid?
      end
    end
  end

  def set_feedbacked
    self.feedbacked = true
    self.save :validate=>false
  end

	def validate_duration
	  if end_at && (start_at > end_at)
	    errors.add(:end_at,"End odometer value must be greater than start odometer value.")
	  end
	end

  def start(klasses_id)
    klasses = klasses_id.map{|klass_id|Klass.find_by_id(klass_id)}

    klasses.delete nil
    
    self.baomings.each do |baoming|
      baoming.destroy unless klasses.include?(baoming.klass)
    end
    
    klasses.each do |klass|
      self.baomings.find_or_create_by_review_id_and_student_id_and_klass_id_and_project_id(
        self,self.student.id,klass.id,project_id
      )
    end

    self.finish
  end

  module BelongsToRelation
    def self.included(base)
      base.class_eval do
        has_many :reviews
      end
    end
  end

  module ReviewList
    def list_reviewers(project)
      list_allowed_to('reviews','start',project)
    end

    def list_allowed_to(ctrl,action,project)
      arr = []
      User.all.each do |user|
        if user.allowed_to?({:controller=>ctrl,:action=>action},project)
          arr.push user
        end
      end
      arr
    end
  end
end

Project.send(:include,Review::BelongsToRelation)
User.send(:include,Review::BelongsToRelation)
User.send(:extend,Review::ReviewList)