class Achiefe < ActiveRecord::Base
  attr_accessible :company, :duration, :job, :position,:person_id,:started_at,:ended_at,:title
  belongs_to :person

  # validates :company, :started_at,:ended_at, :job, :position,:person,:presence=>true
  validates :title,:person_id,:presence=>true

  state_machine :state,:initial=>:unfinished do
  	state :unfinished do
  	end

  	state :finished do
  	end

  	event :finish do
  		transition any=>:finished
  	end

  	event :unfinish do
  		transition any=>:unfinished
  	end
  end

  state_machine.states.map do |state|
    scope state.name, :conditions => { :state => state.name.to_s }
  end    

end
