class Task < ActiveRecord::Base	
  attr_accessible :score,:content, :title

  belongs_to :taskable, :polymorphic => true

  validates :title,:content,:score,:presence => true
  validates :score,:numericality => { :greater_than=>0 }

  def self.fix
  	self.fix_kpi
  end

  def self.fix_kpi
  	Klassplanitem.all.each do |i|
  		if i.klassplan.nil?
  			i.destroy
  		end
  	end
  end
end
