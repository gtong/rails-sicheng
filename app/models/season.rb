class Season < ActiveRecord::Base
  attr_accessible :end_at, :start_at, :title

  def self.now
    Season.used.first
  end

  state_machine :state,:initial=>:notused do
    state :notused do 
    end

    state :used do 
    end

    event :use do
      transition :notused=>:used
    end

    event :notuse do
      transition :used=>:notused
    end    

    before_transition :notused=>:used do |instance,transition|
      Season.used.each do |s|
        s.notuse
      end
    end
  end

  state_machine.states.map do |state|
    scope state.name, :conditions => { :state => state.name.to_s }
  end

  def days
    (start_at.to_date..end_at.to_date).to_a
  end

  def days_by_klass(klass)
    Season.now.sort_days_by_klass days , klass
  end

  def sort_days_by_klass days,klass
    dayss = days.group_by {|date|date.cweek}
    dayss.values.map do |days|
      sorts = days.group_by {|a|klass.rule.prior?a}
      (sorts[true] || []) + (sorts[false] || [])
    end.flatten
  end
end
