# encoding: utf-8
class Course < Klass

  attr_accessible :parents_finished_limit ,:parents_finished_or_learning_limit ,:parents_learning_or_planed_limit 

  Menu = %w(行业与岗位类 英语类 领导力类 口才类 商业类 硬件类 应聘类)

	# Menu = %w(规划类 能力类 应聘类)

	Duration = (0.5..3.5).step(0.5)
  Kind = ('A'..'C').to_a
  Style = %w(讲座类 互动类 小组类)
	Capacity = [10,12,15,16,20,24,25,30,42]

	
  validates :duration,:frequency, :presence => true

  validates :capacity,:inclusion=>{:in=>Capacity}
  validates :duration,:inclusion=>{:in=>Duration}
  validates :menu,:inclusion=>{:in=>Menu}
  validates :kind,:inclusion=>{:in=>Kind}
  validates :style,:inclusion=>{:in=>Style}

  validates :score,:cycle,:frequency,:numericality => { :greater_than=>0 }
  validates :after_task, :presence => true,:if=>'need_after_task == true'
  validates :before_task, :presence => true,:if=>'need_before_task == true'

  validates :section,:presence=>true,:numericality => { :greater_than=>0 }
  
  module BelongsToRelation
    def self.included(base)
      base.class_eval do
        has_many :courses
      end
    end
  end

  def baomingable_for_student(student)
    all_parents.each do |parent|
      return false unless student.klasses.payed.include?parent
    end
    true
  end

  def self.baomingables_for_student(student)
    Course.all.select {|course|course.baomingable_for_student(student)}
  end
end

Project.send(:include,Course::BelongsToRelation)