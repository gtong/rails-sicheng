# encoding: utf-8
class Klass < ActiveRecord::Base
  attr_accessible :color,:category, :capacity_allowed,:room_id,:menu,:laser,:capacity, :chaban, :cycle, :duration, :frequency, :kind, :mic, :network, :projector, :remark, :score, :section, :sound, :state, :style, :title, :tools, :video_makeup,:need_before_task,:need_after_task,:before_task,:after_task,:parents_id,:rule,:rule_id,:description
  attr_accessible :tasks_attributes ,:price
  Colors = ['#a52b24','#4b9e49','#5f6eab','#f5bf21','#de4a8b','#f81910','#3aa55a','#29b5ce','#278070','#ef0ddc','#f5bf21','#cc2b24','#3a3184','#9c5229','#738cc5','#84ad31','#f79463','#8c9ca5','#efce19','#a5348f']
  belongs_to :rule

  has_and_belongs_to_many :teachers,:uniq => true    
  has_and_belongs_to_many :rooms,:uniq=>true
  has_many :klassplans
  has_many :baomings

  has_many :parents_joins,:class_name=>Klassjoin,:foreign_key=>:child_id
  has_many :parents,:through=>:parents_joins

  has_many :children_joins,:class_name=>Klassjoin,:foreign_key=>:parent_id
  has_many :children,:through=>:children_joins

  has_many :tasks, :as => :taskable

  belongs_to :project

  accepts_nested_attributes_for :tasks

  validates :title,:menu,:teachers,:rooms,:price,:presence => true

  validates :price, :numericality => {:greater_than_or_equal_to => 0, :allow_nil => false}

  class SelectGroup
    attr_accessor :title,:items
  end

  def self.select_group
    arr = []
    self::Menu.each do |menu|
      json = SelectGroup.new
      json.title = menu
      json.items = self.where(:menu=>menu)
      arr.push json
    end
    arr
  end

  def self.meet_limit
    all.select {|i|i.meet_limit}
  end

  def auto_planing options={}
    options = {:rule=>self.rule,:teachers=>self.teachers,:students=>[],:rooms=>self.rooms}
    options[:range] = Season.first
  end

  def calendar_events start_at=DateTime.new(2000,1,1),end_at=DateTime.new(3000,1,1)
    kpis = Klassplanitem.where("klass_id = ? and start_at>= ? and end_at <= ?",self.id,start_at,end_at)
    Klassplanitem.calendar_events(kpis)
  end

  def meet_limit
    parents_finished.size >= parents_finished_limit &&
    parents_finished_or_learning.size >= parents_finished_or_learning_limit &&
    parents_learning_or_planed.size >= parents_learning_or_planed_limit
  end

  def all_parents
    collection = parents
    parents.each do |parent|
      collection = collection | parent.all_parents
    end
    collection
  end

  def parents_finished
    baomings.payed.select do |baoming|
      finished = baoming.student.baomings.finished.collect {|i|i.klass}
      (finished & all_parents).sort_by {|i|i.id} == all_parents.sort_by {|i|i.id}
    end
  end

  def parents_finished_or_learning
    baomings.payed.select do |baoming|
      finished = baoming.student.baomings.finished.collect {|i|i.klass}
      learning = baoming.student.baomings.learning.collect {|i|i.klass}

      (finished & all_parents).sort_by {|i|i.id} != all_parents.sort_by {|i|i.id} && 

      ((finished+learning) & all_parents).sort_by {|i|i.id} == all_parents.sort_by {|i|i.id}
    end
  end

  def parents_learning_or_planed
    baomings.payed.select do |baoming|
      learning = baoming.student.baomings.learning.collect {|i|i.klass}
      planed = baoming.student.baomings.planed.collect {|i|i.klass}

      (learning & all_parents).sort_by {|i|i.id} != all_parents.sort_by {|i|i.id} && 

      ((learning+planed) & all_parents).sort_by {|i|i.id} == all_parents.sort_by {|i|i.id}
    end
  end

  # 排课时 ， 所以可排课学员的列表
  # 插班时候也使用这个列表
  def paike_list
    baomings.payed+baomings.unpayed
  end

  # 排课时 ， 所以待补课学员的列表
  def buke_list
    baomings_id = []
    klassplans.each do |kp|
      (kp.baomings_klassplanitems.canced + kp.baomings_klassplanitems.leaved + kp.baomings_klassplanitems.uncompleted).each do |bm_kpi|
        baomings_id.push bm_kpi.baoming_id
      end
    end
    Baoming.where(:id=>baomings_id)
  end

end

