class Attach < ActiveRecord::Base
  attr_accessible :attachable_id, :attachable_type, :file_content_type, :file_file_name, :file_file_size, :file_uploaded_at,:state
  belongs_to :attachable,:polymorphic => true

  attr_accessible :file
  has_attached_file :file

 	state_machine :state,:initial=>:file do
 		state :image do
 		end
 		state :file do
 		end
 	end

 	state_machine.states.map do |state|
    scope state.name, :conditions => { :state => state.name.to_s }
  end
end
