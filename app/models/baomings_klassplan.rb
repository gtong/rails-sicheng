# encoding: utf-8
# klassplan_type
# 表明这个排课相对于某个具体的人的类型 ， ：普通排课/插班/补课
# 
class BaomingsKlassplan < ActiveRecord::Base
  KlassplanType = %w(paike chaban buke)
  attr_accessible :baoming_id, :klassplan_id,:klassplan_type
  belongs_to :baoming
  # belongs_to :student
  belongs_to :klassplan
  has_many :baomings_klassplanitems
  has_many :notifications,:as=>:notificatable
  validates :klassplan_type,:inclusion=>{:in=>KlassplanType}


  state_machine :state,:initial=>'unplaned' do 
    state 'canced' do
    end

  	state 'unplaned' do

  	end

  	state 'rejected' do

  	end

  	state 'planing' do 
      validates :klassplan,:presence=>true
      def klassplanable?
        true
      end
    end

    state 'waiting_confirm' do
      def klassplanable?
        '等待确认'
      end
    end

    state 'unconfirmed' do 
      def klassplanable?
        true
      end
    end

    state 'planed' do
      def klassplanable?
        false
      end
    end

    state 'learning' do
      def klassplanable?
        false
      end
    end

    state 'finished' do
      def klassplanable?
        false
      end
    end

    state 'uncompleted' do
      def klassplanable?
        false
      end
    end

    # 取消排课
    event 'unplan' do
      transition 'planing'=>'unplaned'
    end

    # 开始排课
    # baomings_klassplanitems 变成 planing
    event 'plan' do
      transition ['unplaned']=>'planing'
    end

    # 等待确认
    # 会使 baomings_klassplanitems 都变为 wait_confirm 状态
    event 'wait_confirm' do
      transition 'planing'=>'waiting_confirm'
    end

    # 学员不同意排课
    event 'unconfirm' do
      transition 'waiting_confirm'=>'rejected'
    end

    # 学员同意排课
    # baomings_klassplanitems 变成 planed
    event 'confirm' do
      transition 'waiting_confirm'=>'planed'
    end

    # 排课学习中
    event 'learn' do
      transition 'planed'=>'learning'
    end

    # 课程学习结束
    event 'finish' do
      transition 'learning'=>'finished'
    end

    # 课程未完成 
    event 'uncomplete' do 
      transition 'learning'=>'uncompleted'
    end

    # 退课 
    event 'cancel' do 
      transition 'planed'=>'canced'
    end

    after_transition 'planed'=>'canced' do |instance,transition|
      instance.baomings_klassplanitems.each do |bm_kpi|
        bm_kpi.cancel
      end
    end

    # 开始排课，设置好 baomings_klassplanitems 的状态
    after_transition 'unplaned'=>'planing' do |instance,transition|
      instance.plan_kpis
      instance.baomings_klassplanitems.each do |bm_kpi|
        bm_kpi.plan
      end
    end

    # 取消排课后，设置好 baomings_klassplanitems 的状态
    after_transition ['waiting_confirm','planing'] =>'payed' do |instance,transition|
      instance.klassplans = []
      instance.baomings_klassplanitems.destroy_all
    end

    # 等待学员确认后，设置好 baomings_klassplanitems 的状态
    after_transition any=>'waiting_confirm' do |instance,transition|
      instance.baomings_klassplanitems.each do |bm_kpi|
        bm_kpi.wait_confirm
      end
      Notification.create_student_confirm(instance,instance.baoming.student)
    end

    
    after_transition any=>'planed' do |instance,transition|
      # 学员确认后，设置好 baomings_klassplanitems 的状态
      instance.baomings_klassplanitems.each do |bm_kpi|
        bm_kpi.confirm
      end
      # 如果该学员是最后一个 , 确认该排课 . 
      if instance.klassplan.baomings_klassplans.waiting_confirm.blank?
        instance.klassplan.confirm
      end
    end

    after_transition any=>'finished' do |instance,transition|
      instance.convert_student_to_alumna
    end
  end

  state_machine.states.map do |state|
    scope state.name, :conditions => { :state => state.name.to_s }
  end

  # 
  KlassplanType.map do |state|
    scope state, :conditions => { :klassplan_type => state.to_s }
  end  
	#根据报名课程和排课，建立与排课子项的关系
	# 补课 插班 的逻辑也在这里
	# 如果客户补2节课 ， 需要生成两个 baomings_klassplanitems
	# 如果客户不是补课 ， 生成所有的 baomings_klassplanitems
  def plan_kpis numbers = []
    # 得到需要补课的课节
    if numbers.blank?
    	# 已经安排好或需要上的课程 
    	existed_number = (
    		baoming.baomings_klassplanitems.unplaned +
  	  	baoming.baomings_klassplanitems.planing +
  	  	baoming.baomings_klassplanitems.waiting_confirm +
  	  	baoming.baomings_klassplanitems.planed +
  	  	baoming.baomings_klassplanitems.finished + 
        baoming.baomings_klassplanitems.uncompleted_fixed + 
        baoming.baomings_klassplanitems.leaved_fixed 
  	  ).collect {|i|i.klassplanitem.number}
      numbers = (1..(klassplan.klass.section)).to_a - existed_number
    end
    
    (baoming.baomings_klassplanitems.canced + baoming.baomings_klassplanitems.leaved + baoming.baomings_klassplanitems.uncompleted).each do |i|
      i.fix_leaved
      i.fix_canced
      i.fix_uncompleted
    end

    # 根据需要创建课节
    klassplan.klassplanitems.each do |kpi|
      if (numbers.include? kpi.number)
        bm_kpi = baomings_klassplanitems.find_or_create_by_baoming_id_and_klassplanitem_id(self.baoming.id,kpi.id)
        if kpi.start_at < DateTime.now
          bm_kpi.uncomplete
        end
      end
      
    end
  end  
  # 确认排课的标题和内容
  def confirm_title
    "#{baoming.klass.title}的排课计划"
  end

  def confirm_content
    klassplan.try(:kpis_detail)
  end
end
