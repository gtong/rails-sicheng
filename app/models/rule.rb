# encoding: utf-8
class Rule < ActiveRecord::Base
	Days = [:mon,:tue,:wed,:thu,:fri,:sat,:sun]
	Priors = [:mon_prior,:tue_prior,:wed_prior,:thu_prior,:fri_prior,:sat_prior,:sun_prior]
	Times = [:mon_start,:mon_end,:tue_start,:tue_end,:wed_start,:wed_end,:thu_start,:thu_end,:fri_start,:fri_end,:sat_start,:sat_end,:sun_start,:sun_end]

  attr_accessible :fri_start, :mon_start, :sat_start, :sun_start, :thu_start, :tue_start, :wed_start,:fri_end, :mon_end, :sat_end, :sun_end, :thu_end, :tue_end, :wed_end
  attr_accessible :mon_prior,:tue_prior,:wed_prior,:thu_prior,:fri_prior,:sat_prior,:sun_prior
	attr_accessible :week_max,:week_min,:title

  validate :mon_start_must_be_before_mon_end,:tue_start_must_be_before_tue_end,:wed_start_must_be_before_wed_end,:thu_start_must_be_before_thu_end,:fri_start_must_be_before_fri_end,:sat_start_must_be_before_sat_end,:sun_start_must_be_before_sun_end

  belongs_to :project
  def mon_start_must_be_before_mon_end
	  return if mon_start.blank? || mon_end.blank?
	  errors.add(:mon_start, "必须大于结束时间") unless DateTime.parse(mon_start) < DateTime.parse(mon_end)
	end

	def tue_start_must_be_before_tue_end
	  return if tue_start.blank? || tue_end.blank?
	  errors.add(:tue_start, "必须大于结束时间") unless DateTime.parse(tue_start) < DateTime.parse(tue_end)
	end
	
	def wed_start_must_be_before_wed_end
	  return if wed_start.blank? || wed_end.blank?
	  errors.add(:wed_start, "必须大于结束时间") unless DateTime.parse(wed_start) < DateTime.parse(wed_end)
	end
	
	def thu_start_must_be_before_thu_end
	  return if thu_start.blank? || thu_end.blank?
	  errors.add(:thu_start, "必须大于结束时间") unless DateTime.parse(thu_start) < DateTime.parse(thu_end)
	end
	
	def fri_start_must_be_before_fri_end
	  return if fri_start.blank? || fri_end.blank?
	  errors.add(:fri_start, "必须大于结束时间") unless DateTime.parse(fri_start) < DateTime.parse(fri_end)
	end
	
	def sat_start_must_be_before_sat_end
	  return if sat_start.blank? || sat_end.blank?
	  errors.add(:sat_start, "必须大于结束时间") unless DateTime.parse(sat_start) < DateTime.parse(sat_end)
	end
	
	def sun_start_must_be_before_sun_end
	  return if sun_start.blank? || sun_end.blank?
	  errors.add(:sun_start, "必须大于结束时间") unless DateTime.parse(sun_start) < DateTime.parse(sun_end)
	end

  def prior? day
    (day.monday? && mon_prior?) ||
    (day.tuesday? && tue_prior?) ||
    (day.wednesday? && wed_prior?) ||
    (day.thursday? && thu_prior?) ||
    (day.friday? && fri_prior?) ||
    (day.saturday? && sat_prior?) ||
    (day.sunday? && sun_prior?)
  end

  def available_ranges day
  	day = day.to_datetime.in_time_zone(Time.zone)
  	day = day.change :hour=>0,:minute=>0
  	if day.monday?
  		_start = mon_start
  		_end = mon_end
  	elsif day.tuesday?
  		_start = tue_start
  		_end = tue_end
  	elsif day.wednesday?
  		_start = wed_start
  		_end = wed_end
  	elsif day.thursday?
  		_start = thu_start
  		_end = thu_end
  	elsif day.friday?
  		_start = fri_start
  		_end = fri_end
		elsif day.saturday?
  		_start = fri_start
  		_end = fri_end  		
  	elsif day.sunday?
  		_start = sun_start
  		_end = sun_end
  	end
  	_start = day.change :hour=>_start.split(':')[0].to_i,:minute=>_start.split(':')[1].to_i
  	_end = day.change :hour=>_end.split(':')[0].to_i,:minute=>_end.split(':')[1].to_i
  	[
  		[_start,_end]
  	]
  end

  module BelongsToRelation
    def self.included(base)
      base.class_eval do
        has_many :rules
      end
    end
  end
end

Project.send(:include,Rule::BelongsToRelation)