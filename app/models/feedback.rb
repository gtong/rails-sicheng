class Feedback < ActiveRecord::Base
  attr_accessible :content, :title, :user_id,:student_id
  validates :content,:student_id,:presence=>true
  belongs_to :student
end
