class Busytime < ActiveRecord::Base
  belongs_to :busyable,:polymorphic => true
  scope :by_day, lambda { |dt| where(:from=>dt..(dt+1.day),:to=>dt..(dt+1.day)) }
  module HasManyRelation
  	def self.included(base)
      base.class_eval do
        has_many :busytimes,:as=>:busyable
      end
    end
  end

  module HasOneRelation
  	def self.included(base)
      base.class_eval do
        has_one :busytime,:as=>:busyable
      end
    end
  end

  module UserRelation
  	def self.included(base)
      base.class_eval do
        has_many :busytimes,:through=>:reviews
      end
    end
	end

  module UserInclude
    def unavailable_range_by_day(day)
      day = day.to_date
      busytimes.by_day(day).order('`from` asc').collect do |bt|
        [bt.from,bt.to]
      end
    end

    def available_range_by_day(day)
      arr = []
      day = day.to_date
      range = unavailable_range_by_day(day)
      if range.blank?
        arr.push [day,day.to_datetime+23.hours+59.minutes]
      else
        last_to = day.to_datetime
        range.each do |i|
          arr.push([last_to,i[0]])
          last_to = i[1]
        end
        arr.push [last_to,day.to_datetime+23.hours+59.minutes]
      end
      arr
    end
  end

  module UserExtend
    def available_reviewers(day,project)
      data = []
      list_reviewers(project).each do |reviewer|
        data.push({
          :user=> reviewer,
          :ranges=> reviewer.available_range_by_day(day)
        })
      end
      data
    end 
  end
end

Review.send(:include,Busytime::HasOneRelation)
User.send(:include,Busytime::UserRelation)
User.send(:include,Busytime::UserInclude)
User.send(:extend,Busytime::UserExtend)

# Teather.send(:include,Busytime::HasManyRelation)
