# encoding: utf-8
class Klassplan < ActiveRecord::Base
  attr_accessible :category,:remark,:before_klass_notification_at, :klass_id, :start_at,:end_at,:teacher_id,:menu,:klassplanitems_attributes,:room_id,:created_at,:updated_at,:number,:klass_type

  KlassType = %w(service course)
  belongs_to :klass
  belongs_to :room
  belongs_to :teacher
  belongs_to :project

  has_many :klassplanitems,:uniq=>true,:dependent=> :destroy
  has_many :baomings_klassplans,:uniq=>true
  has_many :baomings,:through=>:baomings_klassplans,:uniq=>true
  has_many :baomings_klassplanitems,:through=>:baomings,:uniq=>true
  has_many :students,:through=>:baomings,:uniq=>true
  has_many :task_instances,:as=>:task_instancable
  has_many :notifications,:as=>:notificatable
  # has_many :bm_kpis_notifications,:as=>:notificatable,:through=>:baomings_klassplans
  validates :klass,:klass_type,:room_id,:teacher_id, :presence => true
  # validates :start_at, :presence => true
  # validates :room, :presence => true  

  accepts_nested_attributes_for :klassplanitems

  after_save do |record|
    if !record.changes.keys.include? 'state'
      record.baomings_klassplans.each do |baomings_klassplan|
        n = Notification.new
        n.state = 'normal'
        n.notificatable = record
        n.person = baomings_klassplan.baoming.student
        n.title = "#{record.klass.title}修改 "
        n.content = "#{record.remark}"
        n.save
      end
    end
  end

  state_machine :state,:initial=>:planing do
    # 正在排课中
    state :planing do
      def state_zh
        '排课中'
      end
    end

    # 已排课
    state :planed do
      def state_zh
        '已排课'
      end
    end

    # 等待老师确认
    state :waiting_teacher_confirm do 
      def state_zh
        '等待老师确认'
      end
    end

    # 等待学生确认
    state :waiting_student_confirm do 
      def state_zh
        '等待学生确认'
      end
    end

    # 已完成 
    state :finished do
      def state_zh
        '已完成'
      end
    end


    #  开始排课
    event :plan do
      transition :unplaned=>:planing
    end

    # 等待老师确认,发送等待老师确认通知
    event :wait_teacher_confirm do 
      transition [:planing]=>:waiting_teacher_confirm
    end

    # 等待学生确认,发送等待学生确认通知
    event :wait_student_confirm do 
      transition :waiting_teacher_confirm=>:waiting_student_confirm
    end

    # 确认排课 ，会在最后一个学员确认排课后自动执行 ， 也可以让管理员手动执行
    event :confirm do
      transition [:waiting_confirm,:waiting_teacher_confirm,:waiting_student_confirm]=>:planed
    end

    event :unconfirm do
      transition :waiting_confirm=>:plan
    end

    event :finish do
      transition :planed=>:finished
    end

    # 发送等待老师确认通知
    after_transition [:planing]=>:waiting_teacher_confirm do |instance,transition|
      Notification.create_teacher_confirm(instance)
    end

    # 发送学生确认通知
    after_transition [:waiting_teacher_confirm]=>:waiting_student_confirm do |instance,transition|
      instance.baomings_klassplans.each do |baomings_klassplan|
        baomings_klassplan.wait_confirm
      end
    end

    # 创建学员任务
    after_transition any=>:planed do |instance,transition|
      instance.create_task_instances_by_task
    end

    # 确认排课计划 
    after_transition [:waiting_confirm,:waiting_teacher_confirm,:waiting_student_confirm]=>:planed do |instance,transition|
      instance.klassplanitems.each do |kpi|
        kpi.confirm
      end
    end
  end

  state_machine.states.map do |state|
    scope state.name, :conditions => { :state => state.name.to_s }
  end

  def unavailable_teachers include_planing=true
    klass.teachers.reject do |teacher|
      teacher.available_for_klassplan self,include_planing
    end
  end

  def available_teachers include_planing=true
    klass.teachers.select do |teacher|
      teacher.available_for_klassplan self,include_planing
    end
  end

  def unavailable_rooms include_planing=true
    klass.rooms.reject do |room|
      room.available_for_klassplan self,include_planing
    end
  end

  def available_rooms include_planing=true
    klass.rooms.select do |room|
      room.available_for_klassplan self,include_planing
    end
  end

  def end_at
    if klass && start_at
      start_at+klass.duration.day
    else
      DateTime.now+10000.day
    end
  end  

  def self.build_by_klass klass
    klassplan = nil
    klass.teachers.each do |teacher|
      klass.rooms.each do |room|
        unless klassplan
          klassplan = build_by_resource klass , teacher , room 
        end
      end
    end
  end

  def self.plan klass
    kp = Klassplan.new
    kp.klass = klass
    kp.save 
    kp.plan
    kp
  end

  def teacher_confirm_title
    "#{klass.title}的排课计划"
  end

  def teacher_confirm_content
    kpis_detail
  end

  def detail
    <<-EOF
    #{self.kpis_detail}
    EOF
  end

  def kpis_detail
    i = 0
    klassplanitems.collect do |kpi|
     i=i+1 
     "第#{i}节 #{kpi.start_at} -- #{kpi.end_at}"
    end.join("<br/>")
  end

  def students_detail
    baomings.collect do |bm|
     bm.student.try(:username)
    end.join("<br/>")
  end

  def build_kpis_simple

    1.upto(klass.section) do |number|
      kpi = klassplanitems.find_or_initialize_by_number number
      kpi.klass = klassplanitems.first.klass
      kpi.teacher = klassplanitems.first.teacher
      kpi.room = klassplanitems.first.room
      kpi.save
    end
  end

# 使用简单版本 build_kpis_simple 
# 已经不适用改函数 ， kpi 通过表单创建
  def build_kpis start_at=nil,season=nil
    season = Season.now unless season

    if start_at
      days = season.sort_days_by_klass (start_at.to_date..(start_at.to_date+20.days)).to_a,klass
    elsif season
      days  = season.days_by_klass klass
    end

    days.size.times do |i|
      if klassplanitems.count() < klass.section
        build_kpi klass.teachers,klass.rooms,days
      end
    end

    (klass.section - klassplanitems.size).times do 
      kpi = klassplanitems.build
      kpi.number = klassplanitems.size
      kpi.klass = klassplanitems.first.klass
      kpi.teacher = klassplanitems.first.teacher
      kpi.room = klassplanitems.first.room
      kpi.save
    end

    self.teacher = klassplanitems.first.teacher
    self.room = klassplanitems.first.room
    self.save :validate=>false
  end

  def build_kpi teachers,rooms,days
    days.each do |day|
      if klassplanitems.last && day <= klassplanitems.last.start_at.to_date
        next 
      end

      if klassplanitems.last && 
        day.cweek == klassplanitems.last.start_at.to_date.cweek && 
        klassplanitems.collect {|i|i.start_at.to_date.cweek == klassplanitems.last.start_at.to_date.cweek}.count() == klass.rule.week_max
        next
      end

      if klassplanitems.count() < klass.section
        rooms.each do |room|
          teachers.each do |teacher|
            day_ranges = klass.rule.available_ranges day
            teacher_ranges = teacher.available_ranges day
            room_ranges = room.available_ranges day
            ranges = My.intersection day_ranges,teacher_ranges
            ranges = My.intersection ranges,room_ranges

            range = ranges.find do |r|
                      (r[1]-r[0])/60/60 >= klass.duration
                    end

            if range
              kpi = klassplanitems.build
              kpi.number = klassplanitems.size
              kpi.teacher = teacher
              kpi.room = room
              kpi.klass = klass
              kpi.start_at = range[0]
              kpi.end_at = range[0]+ klass.duration.hours
              kpi.save

              return kpi
            end
          end
        end
      end
    end
  end

  def create_task_instances_by_task
    klass.tasks.each do |task|
      attributes = task.attributes.select {|k,i|%(title content score).include? k}
      students.each do |student|
        task_instance = TaskInstance.new(attributes)
        task_instance.student = student
        task_instance.task_instancable = self
        task_instance.valid?
        task_instance.save
      end
    end
  end

  def self.build_by_resource klass,teacher,room
    

    baomings = klass.baomings.payed.order('id asc').limit(klass.capacity_allowed)

    days = Season.now.days_by_klass klass

    days.each do |day|

      if klassplanitems.last && day <= klassplanitems.last.start_at.to_date
        next 
      end

      if klassplanitems.last && 
        day.cweek == klassplanitems.last.start_at.to_date.cweek && 
        klassplanitems.collect {|i|i.start_at.to_date.cweek == klassplanitems.last.start_at.to_date.cweek}.count() == klass.rule.week_max
        next
      end

      if klassplanitems.count() < klass.section
        day_ranges = klass.rule.available_ranges day
        teacher_ranges = teacher.available_ranges day
        room_ranges = room.available_ranges day
        ranges = My.intersection day_ranges,teacher_ranges
        ranges = My.intersection ranges,room_ranges

        range = ranges.find do |r|
                  (r[1]-r[0])/60/60 >= klass.duration
                end

        if range
          kpi = klassplanitems.build
          kpi.teacher = teacher
          kpi.room = room
          kpi.klass = klass
          kpi.start_at = range[0]
          kpi.end_at = range[0]+ klass.duration.hours
          kpi.save

          kpi.baomings = baomings
        end
      end
    end
  end

  def self.start_auto klass,season=nil
    season ||= Season.now  
    if klass.meet_limit
      @klassplan = klass.project.klassplans.new
      @klassplan.klass_id = klass.id
      @klassplan.category = klass.class.to_s.downcase
      @klassplan.save :validate=>false
      @klassplan.build_kpis nil,season
      @klassplan.baomings << klass.baomings[0..(klass.section-1)]
      @klassplan.baomings.each {|bm|bm.plan}
      @klassplan.autoplan
    end
  end

  # 取消排课 ， 删除与该次排课的所有数据 ，并修改报名的状态。
  def cancel
    
  end

  module BelongsToRelation
    def self.included(base)
      base.class_eval do
        has_many :klassplans
      end
    end
  end  

  def paike_list
    # TODO:需要剔除补课所关联的报名
    if self.klass
      Baoming.where(:id=>self.baomings_klassplans.paike.collect {|i|i.baoming_id}) + 
      self.klass.paike_list.uniq
    else
      # 理论上 ， 不会走到这里。排课过程中， 所有排课都有课程
      self.baomings.uniq
    end
  end

  def buke_list
    # TODO:需要剔除补课所关联的报名
    if self.klass
      Baoming.where(:id=>self.baomings_klassplans.buke.collect {|i|i.baoming_id}) + 
      self.klass.buke_list.uniq
    else
      # 理论上 ， 不会走到这里。排课过程中， 所有排课都有课程
      self.baomings.uniq
    end
  end

  def chaban_list
    # TODO:需要剔除补课所关联的报名
    if self.klass
      Baoming.where(:id=>self.baomings_klassplans.chaban.collect {|i|i.baoming_id}) + 
      self.klass.paike_list.uniq
    else
      # 理论上 ， 不会走到这里。排课过程中， 所有排课都有课程
      self.baomings.uniq
    end
  end

  def all_notifications
    notifications_id = (notifications + bm_kpis_notifications).collect {|n|n.id}
    Notification.where(:id=>notifications_id)
  end

  def bm_kpis_notifications
    notifications_id = baomings_klassplans.collect do |item|
                        item.notifications
                      end.flatten.collect{|i|i.id}
    Notification.where(:id=>notifications_id)
  end
end

Project.send(:include,Klassplan::BelongsToRelation)