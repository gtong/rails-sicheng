module AvailableRange
	def available_ranges day,include_planing=true
    kpis = klassplanitems.where(:start_at=>(day..day+1.day),:start_at=>(day..day+1.day)).order(:start_at)
    kpis = kpis.reject {|kpi|kpi.klassplan.planing?} unless include_planing    
    ranges = kpis.collect {|kpi|[kpi.start_at,kpi.end_at]}
    My.available_ranges_by_unavailable_ranges day,ranges
  end

  def available_at_range range,include_planing=true
    return true if range[0].nil? || range[1].nil?
    return true if range[0].to_date != range[1].to_date
    ranges = available_ranges range[0].to_date,include_planing
    My.intersection(ranges ,[range]) == [range]
  end

  def available_for_klassplan klassplan,include_planing=true
    collection = klassplan.klassplanitems
    collection.each do |kpi|
      range = [kpi.start_at,kpi.end_at]
      return false unless available_at_range range,include_planing
    end
    return true
  end
end