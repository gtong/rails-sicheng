# encoding: utf-8
class Question < ActiveRecord::Base
  attr_accessible :description,:state, :title,:review_id,:judge_choice,:page
  attr_accessible :choices_attributes
  STATE_EN = %w(single multi judge survey)
  STATE_ZH = %w(单选题 多选题 判断题 调查题)

  belongs_to :review
  has_many :choices,:dependent=>:destroy

  validates :title,:state,:review_id,:presence=>true

  accepts_nested_attributes_for :choices, :allow_destroy => true  
  before_validation :en_state

  state_machine :state,:initial=>:single do
  	state :single do
      
  	end

  	state :multi do
  		
  	end

  	state :judge do
  		
  	end

  	state :survey do
  		
  	end
  end

  before_save :en_state

  def en_state
  	self.state = STATE_EN[STATE_ZH.index(self.state)] if STATE_ZH.index(self.state)
  end

  def zh_state
    self.state = STATE_ZH[STATE_EN.index(self.state)] if STATE_EN.index(self.state)
  end
end
