# encoding: utf-8
class Fake
	def self.project
		Project.find(2)
	end

	def self.create_students(total = 10,project=Fake.project)
		total.times do |i|
			id = Person.last.try(:id) || 0
			s = Student.new
			s.login = "login#{id+1}"
			s.username = "Student #{id+1}"
			s.phone = "13654265#{id+1}".to_i
			s.project = project 
			p s.errors.full_messages unless  s.valid?
			s.save
		end
	end

	def self.create_teachers(total = 10,project=Fake.project)
		total.times do |i|
			id = Person.last.try(:id) || 0
			s = Teacher.new
			s.login = "login#{id+1}"
			s.username = "Teacher #{id+1}"
			s.phone = "13654265#{id+1}".to_i
			s.project = project
			p s.errors.full_messages unless  s.valid?
			s.save
		end
	end

	def self.create_courses(total = 10,project=Fake.project)
		total.times do |i|
			id = Klass.last.try(:id) || 0
			c = Course.new
			c.section = rand(20)
			c.project = project
			c.title = "Course #{id+1}"
			c.menu = Course::Menu[rand(Course::Menu.size)]
			c.rule = Rule.random
			c.parents = Course.random(rand(5))
			c.teachers = Teacher.random(3)
			c.rooms = Room.random(3)
			c.parents_finished_limit = 10
			
			c.duration = Course::Duration[rand(Course::Duration.size)]
			c.capacity = Course::Capacity[rand(Course::Capacity.size)]
			c.kind = Course::Kind[rand(Course::Kind.size)]
			c.style = Course::Style[rand(Course::Style.size)]
			c.capacity_allowed = [c.capacity-5,1].max

			c.score = rand(10)
			c.cycle = rand(10)
			c.frequency = rand(100)

			p c.errors.full_messages unless  c.valid?
			c.save
		end
	end

	def self.create_services(total = 10,project=Fake.project)
		total.times do |i|
			id = Klass.last.try(:id) || 0
			s = Service.new 
			s.project = project
			s.section = rand(20)
			s.title = "Service #{id+1}"
			s.menu = Service::Menu[rand(Service::Menu.size)]
			s.rule = Rule.random
			s.teachers = Teacher.random(3)
			s.rooms = Room.random(3)
			s.capacity = Service::Capacity[rand(Service::Capacity.size)]
			p s.errors.full_messages unless  s.valid?
			s.save
		end
	end

	def self.create_baomings(students,klasses,project=Fake.project)
		students.each do |student|
			klasses.each do |klass|
				bm = Baoming.new
				bm.student = student
				bm.klass = klass
				bm.project = project

				bm.important = Baoming::Important[rand(Baoming::Important.size)]
				bm.end_at = DateTime.new(2014,4,4)
				bm.makeup = Baoming::Makeup[rand(Baoming::Makeup.size)]
				if rand(100)%2 == 0 
					bm.payment_way = Baoming::PaymentWay[rand(Baoming::PaymentWay.size)]
					bm.discount_way = Baoming::DiscountWay[rand(Baoming::DiscountWay.size)]
					bm.free = false
				else
					bm.free = true					
				end
				bm.save
				bm.pay
			end
		end
	end

	def self.create_achieves times=3
		Student.all.each do |s|
			rand(times).times do |i|
				a = Achiefe.new
				a.company = "company#{i}"
				a.position = "position#{i}"
				a.job = "job#{i}"
				a.started_at = DateTime.now
				a.ended_at = DateTime.now+10.days
				a.person = s
				a.save
			end
		end
	end

	def self.create_tasks times=3
		Student.all.each do |s|
			rand(times).times do |i|
				a = TaskInstance.new
				a.student = s
				a.title = "title_#{i}"
				a.content = "content_#{i}"
				a.task_instancable = Klassplan.random
				a.score = rand(10)
				a.save
			end
		end
	end

	def self.create_candidates times=3
		Student.all.each do |s|
			rand(times).times do |i|
				a = Candidate.new
				a.student = s
				a.company = "company#{i}"
				a.position = "position#{i}"
				a.applied_at = DateTime.now
				a.save
			end
		end
	end

	def self.create_scores times=3
		Score.delete_all
		Student.all.each do |s|
			rand(times).times do |i|
				a = Score.new
				a.title = "Title#{i}"
				a.student = s
				a.value = rand(15)
				a.save
			end
		end
	end
end