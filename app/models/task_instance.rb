class TaskInstance < ActiveRecord::Base
  attr_accessible :content, :score, :state, :student_id, :task_instancable_id, :task_instancable_type, :title
  belongs_to :task_instancable, :polymorphic => true
  belongs_to :student

  validates :title,:content,:score,:presence => true
  validates :score,:numericality => { :greater_than=>0 }

  state_machine :state,:initial=>:unfinished do
  	state :unfinished do
  	end

  	state :finished do
  	end

  	event :finish do
  		transition :unfinished=>:finished
  	end

  	event :unfinish do
  		transition :finished=>:unfinished
  	end
  end

  state_machine.states.map do |state|
    scope state.name, :conditions => { :state => state.name.to_s }
  end  
end
