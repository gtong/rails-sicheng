# encoding: utf-8
# state : 确认该节课的状态
# 添加新状态时 ， 需要把不需要排课的状态加到
# 
class BaomingsKlassplanitem < ActiveRecord::Base
  attr_accessible :baoming_id, :klassplanitem_id, :state
  belongs_to :baoming
  belongs_to :klassplanitem
  belongs_to :baoming_klassplan


  state_machine :state,:initial=>'unplaned' do 
    # 不一定有这种状态
  	state 'unplaned' do 
      def state_zh
        '未排课'
      end
    end

    state 'canced' do 
      def state_zh
        '已退课'
      end
    end

    # 正在排课 , 未确认
    state 'planing' do 
      def state_zh
        '已排课'
      end
    end
    # 等待确认
    state 'waiting_confirm' do
      def state_zh
        '确认中'
      end
    end
    # 已排课
    state 'planed' do
      def state_zh
        '已排课'
      end
    end
    # 已请求请假
    state 'requested_leave' do
      def state_zh
        '请假中'
      end
    end
    # 已请假
    state 'leaved' do
      def state_zh
        '已请假'
      end
    end
    # 已完成
    state 'finished' do
      def state_zh
        '已完成'
      end
    end
    # 缺课
    state 'uncompleted' do
      def state_zh
        '旷课'
      end
    end
    # 旷课已补课
    state 'uncompleted_fixed' do
      def state_zh
        '旷课(已补课)'
      end
    end
    # 请假已补课
    state 'leaved_fixed' do
      def state_zh
        '请假(已补课)'
      end
    end

    event 'plan' do 
  		transition 'unplaned'=>'planing'
    end

    event 'wait_confirm' do 
    	transition 'planing'=>'waiting_confirm'
    end

# 确认排课
    event 'confirm' do
      transition ['waiting_confirm']=>'planed'
    end
# 请求请假
    event 'request_leave' do
      transition 'planed'=>'requested_leave'
    end
# 请假
    event 'leave' do
      transition any=>'leaved'
    end
# 签到
    event 'finish' do
      transition any=>'finished'
    end
# 缺课
    event 'uncomplete' do 
      transition any=>'uncompleted'
    end
# 
    event 'fix_leaved' do
      transition 'leaved'=>'leaved_fixed'
    end

    event 'fix_canced' do
      transition 'canced'=>'canced_fixed'
    end

    event 'fix_uncompleted' do
      transition 'uncompleted'=>'uncompleted_fixed'
    end

    # 退课 
    event 'cancel' do 
      transition 'planed'=>'canced'
    end
  end

  state_machine.states.map do |state|
    scope state.name, :conditions => { :state => state.name.to_s }
  end

end
