class Candidate < ActiveRecord::Base
  attr_accessible :applied_at, :company, :passed_at, :position, :state,:duration,:process,:area
  attr_accessible :one_start_at
  attr_accessible :one_end_at
  attr_accessible :one_state
  attr_accessible :two_start_at
  attr_accessible :two_duration
  attr_accessible :two_state
  attr_accessible :three_type
  attr_accessible :three_start_at
  attr_accessible :three_duration
  attr_accessible :three_state
  attr_accessible :lun1_start_at
  attr_accessible :lun1_duration
  attr_accessible :lun1_state
  attr_accessible :lun2_start_at
  attr_accessible :lun2_duration
  attr_accessible :lun2_state
  attr_accessible :lun3_start_at
  attr_accessible :lun3_duration
  attr_accessible :lun3_state
  attr_accessible :lun4_start_at
  attr_accessible :lun4_duration
  attr_accessible :lun4_state
  attr_accessible :lun5_start_at
  attr_accessible :lun5_duration
  attr_accessible :lun5_state
  attr_accessible :lun6_start_at
  attr_accessible :lun6_duration
  attr_accessible :lun6_state
  attr_accessible :lun7_start_at
  attr_accessible :lun7_duration
  attr_accessible :lun7_state
  attr_accessible :lun8_start_at
  attr_accessible :lun8_duration
  attr_accessible :lun8_state
  attr_accessible :lun9_start_at
  attr_accessible :lun9_duration
  attr_accessible :lun9_state
  attr_accessible :lun10_start_at
  attr_accessible :lun10_duration
  attr_accessible :lun10_state

  attr_accessible :lun1_description
  attr_accessible :lun2_description
  attr_accessible :lun3_description
  attr_accessible :lun4_description
  attr_accessible :lun5_description
  attr_accessible :lun6_description
  attr_accessible :lun7_description
  attr_accessible :lun8_description
  attr_accessible :lun9_description
  attr_accessible :lun10_description

  attr_accessible :lun1_type
  attr_accessible :lun2_type
  attr_accessible :lun3_type
  attr_accessible :lun4_type
  attr_accessible :lun5_type
  attr_accessible :lun6_type
  attr_accessible :lun7_type
  attr_accessible :lun8_type
  attr_accessible :lun9_type
  attr_accessible :lun10_type

  belongs_to :student
  validates :duration,:company,:student_id,:presence=>true

  state_machine :state,:initial=>:unpassed do 
    state :unpassed do
    end

    state :passed do
    end

    event :pass do
      transition :unpassed=>:passed
    end

    event :unpass do
      transition :passed=>:unpassed
    end

    before_transition :unpassed=>:passed do |instance,transition|
      instance.passed_at = DateTime.now
    end
  end
end
