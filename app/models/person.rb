# encoding: utf-8
class Person < ActiveRecord::Base
  include AvailableRange
  attr_accessible :alumnus, :exam_4, :exam_6, :exam_qimo, :exam_toefl, :finished, :guide, :guide_vip, :home_101, :home_51, :home_summer, :logic, :phone, :qq, :username, :phone, :grade, :signup_openclass,:school,:college,:major,:project
  attr_accessible :login,:password,:password_confirmation,:type,:weixin,:email,:sex,:qzsj

  attr_accessible :attaches_attributes
  has_many :attaches,:as=>:attachable
  accepts_nested_attributes_for :attaches, :allow_destroy => true    

  has_many :warnings
  has_many :notifications
  has_many :positions
  has_many :achieves
  belongs_to :project
  # has_many :markets_people
  # has_many :markets,:through=>:markets_people
  has_and_belongs_to_many :klasses,:uniq => true
  has_and_belongs_to_many :klassplans,:uniq => true  

  Sex = ['男','女']
  validates :username,:presence=>true
  validates_confirmation_of  :password
  validates :sex,:inclusion=>{:in=>Sex},:unless=>'sex.blank?'

  before_save :skip_password

  def skip_password
    if self.changes && self.changes['password'] && self.changes['password'][1].blank?
      self.password = changes['password'][0]
    end
  end

  def self.authenticate(login, password)
    return nil  unless user = find_by_login(login)
    return user if     user.authenticated?(password)
  end

  def authenticated?password
    user.password == password
  end

  def as_json(options={})
    super({:except=>[:password]}.merge options)
  end
end

 