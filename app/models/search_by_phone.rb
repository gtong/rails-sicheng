class SearchByPhone
  include ActiveModel::Validations
	include ActiveModel::Conversion

  validates_presence_of :phone
  attr_accessor :phone

  def initialize(attributes = {})
    attributes.each do |name, value|
      send("#{name}=", value) if respond_to?"#{name}="
    end
  end

  def persisted?
    false
  end

  def search_student
    Student.find_by_phone(self.phone)
  end

  def search_teacher
    Teacher.find_by_phone(self.phone)
  end
end