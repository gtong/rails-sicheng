# 批量付款
class BaomingPay

  include ActiveModel::Validations
  include ActiveModel::Conversion
  extend ActiveModel::Naming

  attr_accessor :student_id,:discount_way,:payment_way,:makeup,:next,:important,:installment,:installment_duration,:installment_value,:installment_last,:created_at,:updated_at,:review_id,:free,:klass_id,:end_at,:state,:project_id,:baominged_at,:price

  attr_accessor :baomings
  def initialize(attributes = {})
    attributes.each do |name, value|
      send("#{name}=", value)
    end
  end

  def set_baomings baomings
    self.baomings=baomings
    initialize_from_baomings
  end

  def initialize_from_baomings
    self.price = self.baomings.collect {|b|b.price}.compact.inject(0,:+)
    self.student_id = self.baomings.first.student_id
  end

  def persisted?
    false
  end

  
    
  # include ActiveAttr::Model
  # attribute :baomings
  # attribute :name


end