class Score < ActiveRecord::Base
  attr_accessible :student_id, :type, :value,:title
  belongs_to :student
end
