class Teacher < Person
  validates :phone,:presence=>true
  validates :phone,:uniqueness=>true,:allow_nil=>true
  validates :phone,:numericality=>true,:allow_nil=>true
  
  belongs_to :project
  has_many :klassplanitems do 
    # def planed
    #   where("klassplans.state = 'planed'")
    # end

    # def unplaned
    #   where("klassplans.state = 'unplaned'")
    # end
  end
  has_many :klassplans

  def calendar_events start_at=DateTime.new(2000,1,1),end_at=DateTime.new(3000,1,1)
    kpis = Klassplanitem.where("teacher_id = ? and start_at>= ? and end_at <= ?",self.id,start_at,end_at)
    Klassplanitem.calendar_events(kpis)
  end

  module BelongsToRelation
    def self.included(base)
      base.class_eval do
        has_many :teachers
      end
    end
  end
end

Project.send(:include,Teacher::BelongsToRelation)