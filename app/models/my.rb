class My
	def self.available_ranges_by_unavailable_ranges day , range
		arr = []
    day = day.to_datetime.in_time_zone(Time.zone).change :hour=>0,:minute=>0

    if range.blank?
      arr.push [day,day+1.day]
    else
      last_to = day
      range.each do |i|
        arr.push([last_to,i[0]])
        last_to = i[1]
      end
      arr.push [last_to,day+1.day]
    end
    arr.reject {|i|i[0]==i[1]}
	end

	def self.intersection(r2,r1)
		result = []
		r1.each do |i|
			valid =  r2.reject do |j|
				i[0]>=j[1] || j[0]>=i[1]
			end
			
			result = result | valid.collect do |j|
				r = (i+j).sort
				r.pop
				r.shift
				r
			end
		end
		result
	end
end