class StudentFilter < WillFilter::Filter
  def model_class
    Student
  end

  def definition
    defs = super  
    defs[:sex][:is] = :list
    # defs[:sex][:is_not] = :list
    defs
  end

  def value_options_for(condition_key)
    if condition_key == :sex
      return ["male", "female", "unknown"]
    end
    return []
  end
end