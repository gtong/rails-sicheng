class BaomingsKlassplanitemDecorator < Draper::Decorator
  delegate_all

  # Define presentation-specific methods here. Helpers are accessed through
  # `helpers` (aka `h`). You can override attributes, for example:
  #
  #   def created_at
  #     helpers.content_tag :span, class: 'time' do
  #       object.created_at.strftime("%a %m/%d/%y")
  #     end
  #   end
  def state_zh
  	case state
  	when :unplaned 
  		'未排课'
  	when :planing 
  		'排课中'
  	when :waiting_confirm 
  		'等待确认'
  	when :planed 
  		'已排课'
  	when :requested_leave 
  		'请假中'
  	when :leaved 
  		'已请假'
  	when :finished 
  		'已完成'
  	when :uncompleted 
  		'未完成'
  	else
  	end
  end

  def event_zh
  	# case 
  	# when plan
   #  when wait_confirm
   #  when confirm
   #  when request_leave
   #  when leave
   #  when finish
   #  when uncomplete
end
