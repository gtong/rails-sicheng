# encoding: utf-8
module ClientsHelper
	include CommonHelper

  def destroy_multiple_link(project)
    destroy_multiple_project_clients_path(project)
  end

	def view_link(project,client)
		link_to client.username, project_client_path(@project,client)
	end

	def edit_link_mini(project,client)
		link_to t('.edit', :default => t("helpers.links.edit")),
                      edit_project_client_path(@project,client), :class => 'btn btn-mini'
	end

	def destroy_link_mini(project,client)
		 link_to t('.destroy', :default => t("helpers.links.destroy")),
                      project_client_path(project,client),
                      :method => :delete,
                      :data => { :confirm => t('.confirm', :default => t("helpers.links.confirm", :default => 'Are you sure?')) },
                      :class => 'btn btn-mini btn-danger'
  end	

  def duplicate_link_mini(project,client)
    link_to t('duplicate', :default => t("helpers.links.duplicate")),
                      duplicate_project_client_path(@project,client), :class => 'btn btn-mini'
  end

  def edit_link(project,client)
		link_to t('.edit', :default => t("helpers.links.edit")),
                      edit_project_client_path(@project,client), :class => 'btn '
	end

	def destroy_link(project,client)
		 link_to t('.destroy', :default => t("helpers.links.destroy")),
                      project_client_path(project,client),
                      :method => :delete,
                      :data => { :confirm => t('.confirm', :default => t("helpers.links.confirm", :default => 'Are you sure?')) },
                      :class => 'btn  btn-danger'
  end	

  def back_link(project)
  	link_to title,
              project_clients_path(project), :class => 'btn'
  end

  def redirect_link(project,client)
  	redirect_to project_client_path(@project,@client), notice: '客户创建成功'
  end
  
  def title
  	'客户'
  end

  def msg_title
  	'客户'
  end
end
