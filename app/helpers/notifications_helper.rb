# encoding: utf-8
module NotificationsHelper
	def response_msg notification
		unless notification.normal?
			msg = ''
			msg = msg + t(notification.response) if notification.response 
	    msg = msg + link_to('同意',confirm_project_notification_path(@project,notification),:class=>'btn btn-mini') unless notification.response
	    msg = msg + ' '  
	    msg = msg + link_to('不同意',unconfirm_project_notification_path(@project,notification),:class=>'btn btn-mini') unless notification.response 
	    msg.html_safe
	  end
	end
end
