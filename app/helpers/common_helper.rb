# encoding: utf-8
module CommonHelper
	# def shifou(value)
	# 	return 'yes' if value == true || value == 1
	# 	return 'no' if value == false || value == 0
	# 	''
	# end
	def feedbacked(review)
		review.feedbacked ? '已反馈' : '未反馈'
	end

	def set_feedbacked_link(project,review)
		unless review.feedbacked
			link_to '设置为反馈',set_feedbacked_project_review_path(project,review),:class=>'btn btn-mini'
		else
			feedbacked(review)
		end
	end

	def shifou(value)
		return '是' if value == true || value == 1
		return '否' if value == false || value == 0
		''
	end

	def need(value)
		return '需要' if value == true || value == 1
		return '不需要' if value == false || value == 0
		''
	end

	def keyi(value)
		return '可以' if value == true || value == 1
		return '不可以' if value == false || value == 0
		''
	end

	def youxian(value)
		return '优先' if value == true || value == 1
		return '不优先' if value == false || value == 0
		''
	end

	class MyOption
		attr_accessor :label
		attr_accessor :value
	end

	def i18n_collection(keys)
		keys.map do |key|
			s = MyOption.new
			s.label = t key
			s.value = key
			s 
		end
	end

	def percentage(value)
		"#{value.round(3)*100}%"
	end

	def response_msg notification
		unless notification.normal?
			msg = ''
			msg = msg + t(notification.response) if notification.response 
	    msg = msg + link_to('同意',confirm_project_notification_path(@project,notification),:class=>'btn btn-mini') unless notification.response
	    msg = msg + ' '  
	    msg = msg + link_to('不同意',unconfirm_project_notification_path(@project,notification),:class=>'btn btn-mini') unless notification.response 
	    msg.html_safe
	  end
	end

end