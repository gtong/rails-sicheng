# encoding: utf-8
module AlumnasHelper
	include CommonHelper

  def destroy_multiple_link(project)
    destroy_multiple_project_alumnas_path(project)
  end

	def view_link(project,alumna)
		link_to alumna.username, project_alumna_path(@project,alumna)
	end

	def edit_link_mini(project,alumna)
		link_to t('.edit', :default => t("helpers.links.edit")),
                      edit_project_alumna_path(@project,alumna), :class => 'btn btn-mini'
	end

	def destroy_link_mini(project,alumna)
		 link_to t('.destroy', :default => t("helpers.links.destroy")),
                      project_alumna_path(project,alumna),
                      :method => :delete,
                      :data => { :confirm => t('.confirm', :default => t("helpers.links.confirm", :default => 'Are you sure?')) },
                      :class => 'btn btn-mini btn-danger'
  end	

  def duplicate_link_mini(project,alumna)
    link_to t('duplicate', :default => t("helpers.links.duplicate")),
                      duplicate_project_alumna_path(@project,alumna), :class => 'btn btn-mini'
  end

  def edit_link(project,alumna)
		link_to t('.edit', :default => t("helpers.links.edit")),
                      edit_project_alumna_path(@project,alumna), :class => 'btn '
	end

	def destroy_link(project,alumna)
		 link_to t('.destroy', :default => t("helpers.links.destroy")),
                      project_alumna_path(project,alumna),
                      :method => :delete,
                      :data => { :confirm => t('.confirm', :default => t("helpers.links.confirm", :default => 'Are you sure?')) },
                      :class => 'btn  btn-danger'
  end	

  def back_link(project)
  	link_to title,
              project_alumnas_path(project), :class => 'btn'
  end

  def redirect_link(project,alumna)
  	redirect_to project_alumna_path(@project,@alumna), notice: '客户创建成功'
  end
  
  def title
  	'校友'
  end

  def msg_title
  	'校友'
  end
end
