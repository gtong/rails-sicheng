# encoding: utf-8
module StudentsHelper
	include CommonHelper
  
  def destroy_multiple_link(project)
    destroy_multiple_project_students_path(project)
  end

	def person_klass_status(person,klass)
		bm = person.baomings.where(:klass_id=>klass.id).first
		return I18n.t :nosignup unless bm
		I18n.t bm.state
	end

	def view_link(project,student)
		link_to student.username, project_student_path(@project,student)
	end

	def edit_link_mini(project,student)
		link_to t('.edit', :default => t("helpers.links.edit")),
                      edit_project_student_path(@project,student), :class => 'btn btn-mini'
	end

  def duplicate_link_mini(project,student)
    link_to t('duplicate', :default => t("helpers.links.duplicate")),
                      duplicate_project_student_path(@project,student), :class => 'btn btn-mini'
  end

	def destroy_link_mini(project,student)
		 link_to t('.destroy', :default => t("helpers.links.destroy")),
                      project_student_path(project,student),
                      :method => :delete,
                      :data => { :confirm => t('.confirm', :default => t("helpers.links.confirm", :default => 'Are you sure?')) },
                      :class => 'btn btn-mini btn-danger'
  end

  def edit_link(project,student)
		link_to t('.edit', :default => t("helpers.links.edit")),
                      edit_project_student_path(@project,student), :class => 'btn '
	end

	def destroy_link(project,student)
		 link_to t('.destroy', :default => t("helpers.links.destroy")),
                      project_student_path(project,student),
                      :method => :delete,
                      :data => { :confirm => t('.confirm', :default => t("helpers.links.confirm", :default => 'Are you sure?')) },
                      :class => 'btn  btn-danger'
  end

  def back_link(project)
  	link_to t('.back', :default => t("helpers.links.back")),
              project_students_path(project), :class => 'btn'
  end

  def redirect_link(project,student)
  	redirect_to project_student_path(@project,@student), notice: '学员创建成功'
  end

  def title
  	'学员'
  end

  def msg_title
  	'学员'
  end
end