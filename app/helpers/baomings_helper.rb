module BaomingsHelper
	include CommonHelper

	def state_collection
		Baoming::State.map do |key|
			s = MyOption.new
			s.label = t key
			s.value = key
			s 
		end
	end
end
